package m9u3.impl.wallet;

import lib.Startable;
import m9u3.prob.wallet.WalletAsyncClient;
import m9u3.prob.wallet.WalletFactory;
import m9u3.prob.wallet.WalletSyncClient;

public class WalletFactoryImpl implements WalletFactory {

	
	@Override
	public WalletSyncClient createSyncClient(String host, int port) {
		return new WalletSyncClientImpl(host, port);
	}

	@Override
	public WalletAsyncClient createAsyncClient(String host, int port) {
		return new WalletAsyncClientImpl(host, port);
	}

	@Override
	public Startable createSingleServer(String host, int port) {
		return new WalletServerImpl.MyStartable(host, port);
	}

	@Override
	public Startable createCoopServer(String host, int port) {
		return new WalletCoopServerImpl(host, port);
	}

}
