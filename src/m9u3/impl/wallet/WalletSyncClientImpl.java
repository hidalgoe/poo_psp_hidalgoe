package m9u3.impl.wallet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.logging.Logger;

import m9u3.prob.wallet.WalletSyncClient;

public class WalletSyncClientImpl implements WalletSyncClient {

	final Logger LOGGER = Logger.getLogger(WalletSyncClientImpl.class.getName());
	final Charset CHARSET = StandardCharsets.UTF_8;
	final int CONNECT_TIMEOUT = 1500;
	final int RESPONSE_TIMEOUT = 1500;
	PrintWriter out;
	BufferedReader in;

	String host;
	int port;
	Socket socket;

	public WalletSyncClientImpl(String host, int port) {
		super();
		this.host = host;
		this.port = port;

	}

	@Override
	public void connect() throws IOException {

		socket = new Socket();		
		socket.connect(new InetSocketAddress(host, port), CONNECT_TIMEOUT);
		socket.setSoTimeout(RESPONSE_TIMEOUT);
		out = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), CHARSET), true);
		in = new BufferedReader(new InputStreamReader(socket.getInputStream(), CHARSET));

	}

	@Override
	public void disconnect() throws IOException {

		socket.close();

	}

	@Override
	public BigDecimal add(BigDecimal bd) {

		String comanda = "+" + bd;
		out.println(comanda);
		String result;
		BigDecimal bigDecimal = null;

		try {
			result = in.readLine();
			LOGGER.info("received " + result);

			if (result.charAt(0) == '0') {

				String maikol = result.replaceFirst("0", "");
				bigDecimal = BigDecimal.valueOf(Double.valueOf(maikol));
			} else  if (result.charAt(0) == '1') {
				throw new IllegalStateException();
			} else {
				throw new IllegalArgumentException();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return bigDecimal;
	}

	@Override
	public BigDecimal take(BigDecimal bd) {

		String comanda = "-" + bd;
		out.println(comanda);
		String result;
		BigDecimal bigDecimal = null;

		try {
			result = in.readLine();
			LOGGER.info("received " + result);

			if (result.charAt(0) == '0') {

				String maikol = result.replaceFirst("0", "");
				bigDecimal = BigDecimal.valueOf(Double.valueOf(maikol));

			} else if (result.charAt(0) == '1') {
				throw new IllegalStateException();
			} else {
				throw new IllegalStateException();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return bigDecimal;
	}

	@Override
	public BigDecimal getBalance() {

		out.println("?");
		BigDecimal bigDecimal = null;

		try {
			String x = in.readLine();
			LOGGER.info("received " + x);

			String maikol = x.replaceFirst("0", "");
			bigDecimal = BigDecimal.valueOf(Double.valueOf(maikol));

		} catch (IOException e) {
			e.printStackTrace();
		}

		return bigDecimal;

	}

}
