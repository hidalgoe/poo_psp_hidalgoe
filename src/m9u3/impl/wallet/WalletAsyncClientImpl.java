package m9u3.impl.wallet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.logging.Level;
import java.util.logging.Logger;

import lib.Asserts;
import m9u3.play.walletb.WalletHelper;
import m9u3.play.walletb.WalletHelper.WalletResult;
import m9u3.prob.wallet.WalletAsyncClient;

public class WalletAsyncClientImpl implements WalletAsyncClient, Runnable {

	 static final Logger LOGGER = Logger.getLogger(WalletAsyncClientImpl.class.getName());
	    
	    static final Charset CHARSET = StandardCharsets.UTF_8; // it could be UTF8, ISO8859_1...
	    static final int CONNECT_TIMEOUT = 1500;
	    static final int RESPONSE_TIMEOUT = 1500;

	    private String host;
	    private int port;
	    private Socket socket;
	    private PrintWriter out;
	    private BufferedReader in;
	    
	    private WalletHandler handler;
	    
	    public WalletAsyncClientImpl(String host, int port) {
	        this.host = host;
	        this.port = port;
	    }
	    
	    @Override
	    public void connect() throws IOException {
	        
	        if (this.handler == null) {
	            throw new RuntimeException("handler is not set");
	        }
	        
	        socket = new Socket();
	        socket.connect(new InetSocketAddress(host, port), CONNECT_TIMEOUT); // connect timeout
	        socket.setSoTimeout(RESPONSE_TIMEOUT); // response timeout
	        
	        out = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), CHARSET), true);
	        in = new BufferedReader(new InputStreamReader(socket.getInputStream(), CHARSET));
	        
	        new Thread(this, "cln" + socket.getLocalPort()).start();
	    }

	    @Override
	    public void disconnect() throws IOException {
	        
	        try {
	            socket.close();
	        } finally {
	            socket = null;
	        }        
	    }

	    @Override
	    public void setHandler(WalletHandler handler) {        
	        this.handler = handler;
	    }

	    @Override
	    public void add(BigDecimal bd) {
	        Asserts.isNotNull(out, "not connected");
	        out.println("+" + bd);
	    }

	    @Override
	    public void take(BigDecimal bd) {
	        Asserts.isNotNull(out, "not connected");
	        out.println("-" + bd);
	    }
	    
	    @Override
	    public void requestBalance() {
	        Asserts.isNotNull(out, "not connected");
	        out.println("?");
	    }

	    @Override
	    public void run() {
	        
	        try {
	            while (true) {
	                try {
	                    String input = in.readLine();
	                    if (input == null) {
	                        LOGGER.warning("server disconnected!");
	                        return;
	                    }
	                                    
	                    LOGGER.info("message is " + input);                  
	                    WalletResult wr = WalletHelper.getResult(input);
	                    if (wr == null) {
	                    	handler.onError(
	                    			new RuntimeException("bad response: " + input));
	                    }
	                    else {
	                        int code = wr.getCode();
	                        if (code == 0) {                       
	                            BigDecimal balance = new BigDecimal(wr.getText());
	                            handler.onSuccess(balance);
	                        }
	                        else if (code == 1) {
	                            handler.onError(
	                            		new IllegalStateException("not enough"));
	                        }
	                        else {
	                            handler.onError(
	                            		new RuntimeException("result code is " + code));
	                        }
	                    }
	                    
	                } catch (SocketTimeoutException e) {
	                    // read timeout: do nothing
	                    LOGGER.fine("timeout reading!");
	                }
	            }
	            
	        } catch (IOException e) {
	            if (socket == null || socket.isClosed()) {
	                LOGGER.info("IOException on close: " + e.getMessage());
	                return;
	            }
	            
	            LOGGER.log(Level.SEVERE, "serving", e);
	            
	        } finally {
	            try {
	                if (socket != null)
	                    socket.close();
	            } catch (IOException e) {
	                LOGGER.warning("closing: " + e.getMessage());
	            }
	        }
	    }
	}

