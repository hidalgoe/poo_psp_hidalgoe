package m9u3.impl.wallet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

import lib.Loggers;
import lib.Startable;
import m9u3.impl.testing.servidor;

public class WalletServerImpl {
	static final Logger LOGGER = Logger.getLogger(WalletServerImpl.class.getName());

	final static Charset CHARSET = StandardCharsets.UTF_8; // it could be UTF8, ISO8859_1...
	final static int RESPONSE_TIMEOUT = 5000;
	static final String TWODEC_NUMBER = "^[0-9]+(\\.[0-9]{1,2})?$";
	static double money = 0;

	public String host;
	int port;
	static Startable s;

	public WalletServerImpl(String hOST, int pORT) {
		super();
		this.host = hOST;
		this.port = pORT;
		s = new MyStartable(host, port);
	}

	public static class MyStartable implements Startable, Runnable {

		// private final DecimalFormat df = new DecimalFormat("0.00");
		private Thread thread;
		private ServerSocket serverSocket;
		private boolean stopping;
		String host;
		int port;

		public MyStartable(String host, int port) {
			this.host = host;
			this.port = port;

		}

		@Override
		public void start() {
			thread = new Thread(this, "startable");
			thread.start();
		}

		@Override
		public void stop() {
			try {
				// close will make the accept() method throw a SocketException
				stopping = true;
				serverSocket.close();

			} catch (IOException e) {
				LOGGER.log(Level.SEVERE, "closing", e);
			}
		}

		@Override
		public boolean isStarted() {
			return thread != null && thread.isAlive();
		}

		@Override
		public void run() {

			LOGGER.info("started!");
			try {
				serverSocket = new ServerSocket(port, 10, InetAddress.getByName(host));

				while (true) {
					try (Socket clientSocket = serverSocket.accept()) {
						LOGGER.info("handle client " + clientSocket);
						handle(clientSocket);
					}
				}

			} catch (IOException e) {
				if (stopping) {
					LOGGER.info("stopping exception is " + e);
				} else {
					LOGGER.log(Level.SEVERE, "accepting", e);
				}

			} finally {
				LOGGER.info("done!");
			}
		}

		void handle(Socket clientSocket) {

			try (PrintWriter out = new PrintWriter(new OutputStreamWriter(clientSocket.getOutputStream(), CHARSET),
					true);
					BufferedReader in = new BufferedReader(
							new InputStreamReader(clientSocket.getInputStream(), CHARSET))) {

				String input;

				// null means end of stream
				while ((input = in.readLine()) != null) {
					LOGGER.info("request is " + input);

					String output;
					try {
						output = getOutput(input);
					} catch (Exception e) {
						LOGGER.log(Level.SEVERE, "getting output", e);
						output = "2 " + e.getMessage();
					}

					LOGGER.info("sending " + output);
					out.println(output);
				}

				LOGGER.info("end of stream from " + clientSocket.getRemoteSocketAddress());

			} catch (SocketTimeoutException e) {
				LOGGER.fine("timeout reading"); // read timeout: do nothing

			} catch (IOException e) {
				LOGGER.log(Level.SEVERE, "handling", e);
			}
		}

		private String getOutput(String input) {

			if (input == "") {
				return "4 Error de sintaxis";
			} else if (input.charAt(0) == '?') {
				return "0 " + money;
			} else {
				if (input.charAt(0) == '+') {

					if (input == "+0") {
						return "1 error al sumar 0";
					}

					LOGGER.info("action is sumar");

					input = input.replace("+", "");

					BigDecimal nick = new BigDecimal(input);

					// return ""+nick;

					long number = nick.longValue();

					if (!Pattern.matches(TWODEC_NUMBER, input)) {
						
						return "4 Mas de 2 decimales";
					}
					try {
						if (number <= 0) {
							return "4 Error de sintaxis";
						}
					} catch (NumberFormatException e) {
						return "4 Error de sintaxis";
					}
					money = money + nick.doubleValue();
					return "0 " + money;

				} else if (input.charAt(0) == '-') {

					if (input == "-0") {
						return "1 error al restar 0";
					}

					if (money <= 0) {
						throw new IllegalStateException();
					}

					LOGGER.info("action is restar");
					input = input.replace("-", "");

					BigDecimal nick = new BigDecimal(input);

					Double number = nick.doubleValue();

					if (!Pattern.matches(TWODEC_NUMBER, input)) {
						return "4 Mas de 2 decimales";
					}

					try {

						if (number < 0) {
							return "4 Error de sintaxis";
						}

						if (number > money) {
							throw new IllegalStateException();
						}
					} catch (NumberFormatException e) {
						return "4 Error de sintaxis";
					}
					
					money = money - number;
					return "0 " + money;
				} else {
					LOGGER.info("action is ERROR " + input);
					return "3 Error de argumento";
				}
			}

		}
	}

}
