package m9u3.impl.wallet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

import lib.Loggers;
import lib.Startable;
import m9u3.play.wallet.BaseSingleServer;
import m9u3.play.walleta.WalletSingleServer;

public class WalletCoopServerImpl implements Startable, Runnable {

	static final Logger LOGGER = Logger.getLogger(BaseSingleServer.class.getName());

	static final Charset CHARSET = StandardCharsets.UTF_8; // it could be UTF8, ISO8859_1...
	static final int RESPONSE_TIMEOUT = 1500;

	static List<MySocketHandle> list = new ArrayList<MySocketHandle>();
	int n;

	private Thread thread;
	private String host;
	private int port;
	private ServerSocket serverSocket;
	static final String TWODEC_NUMBER = "^[0-9]+(\\.[0-9]{1,2})?$";
	static double money = 0;

	public static void main(String[] args) {

		Loggers.configure(Level.INFO);
		new WalletSingleServer("127.0.0.1", 5508).start();

	}

	public WalletCoopServerImpl(String host, int port) {
		this.host = host;
		this.port = port;
	}

	public void start() {
		thread = new Thread(this, "WalletCoop");
		thread.start();
	}

	public void stop() {
		try {
			serverSocket.close();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public boolean isStarted() {
		return thread != null && thread.isAlive();
	}

	public void run() {

		LOGGER.info("starting server at " + host + ":" + port);
		ExecutorService service = Executors.newCachedThreadPool();

		try {
			serverSocket = new ServerSocket(port, 10, InetAddress.getByName(host));
			serverSocket.setSoTimeout(RESPONSE_TIMEOUT);

			while (true) {
				try {

					Socket clientSocket = serverSocket.accept();
					LOGGER.info("accept from " + clientSocket.getRemoteSocketAddress());

					clientSocket.setSoTimeout(RESPONSE_TIMEOUT);
					MySocketHandle msh = new MySocketHandle(clientSocket);
					list.add(msh);

					service.execute(msh);

				} catch (SocketTimeoutException e) {
					LOGGER.fine("timeout accepting"); // timeout in the accept: ignore
				}
			}

		} catch (IOException e) {
			if (serverSocket != null && serverSocket.isClosed()) {
				LOGGER.info("server was closed");
				return;
			}

			LOGGER.log(Level.SEVERE, "accepting", e);

		} finally {
			service.shutdown();
			LOGGER.info("done!");
		}
	}

	public void actualiza(MySocketHandle aesteno, double money) {

		for (int i = 0; i < list.size(); i++) {

			if (list.get(i) != aesteno)
				list.get(i).getOut().println("0 " + money);
		}

	}

	class MySocketHandle implements Runnable {

		private Socket cs;
		PrintWriter out;
		BufferedReader in;

		public MySocketHandle(Socket cs) {

			this.cs = cs;
			try {
				this.out = new PrintWriter(new OutputStreamWriter(cs.getOutputStream(), CHARSET), true);
				this.in = new BufferedReader(new InputStreamReader(cs.getInputStream(), CHARSET));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		public PrintWriter getOut() {

			return out;
		}

		@Override
		public void run() {
			try {

				while (true) {
					try {
						String input = in.readLine();
						if (input == null) {
							LOGGER.info("end of stream from " + cs.getRemoteSocketAddress());
							break;
						}

						try {
							String output = getOutput(input);
							LOGGER.info("el man: " + n + " request is " + input + ", response is " + output);
							out.println(output);
						} catch (RuntimeException e) {
							LOGGER.log(Level.SEVERE, "received " + input + ", internal error", e);
							out.println("4 Error de sintaxis");
						}

					} catch (SocketTimeoutException e) {
						LOGGER.fine("timeout reading"); // read timeout: error
					}
				}

			} catch (IOException e) {
				LOGGER.log(Level.SEVERE, "handling", e);

			} finally {
				try {
					cs.close();
					LOGGER.info("disconnected from " + cs.getRemoteSocketAddress());
				} catch (IOException e) {
					LOGGER.warning("closing: " + e.getMessage());
				}
			}

		}

		private String getOutput(String input) {

			if (input == "") {
				return "4 Error de sintaxis";
			} else if (input.charAt(0) == '?') {
				return "0 " + money;
			} else {
				if (input.charAt(0) == '+') {

					if (input == "+0") {
						return "1 error al sumar 0";
					}

					LOGGER.info("action is sumar");

					input = input.replace("+", "");

					BigDecimal nick = new BigDecimal(input);

					// return ""+nick;

					long number = nick.longValue();

					if (!Pattern.matches(TWODEC_NUMBER, input)) {

						return "4 Mas de 2 decimales";
					}
					try {
						if (number <= 0) {
							return "4 Error de sintaxis";
						}
					} catch (NumberFormatException e) {
						return "4 Error de sintaxis";
					}
					money = money + nick.doubleValue();
					actualiza(this, money);
					return "0 " + money;

				} else if (input.charAt(0) == '-') {

					if (input == "-0") {
						return "1 error al restar 0";
					}

					if (money <= 0) {
						throw new IllegalStateException();
					}

					LOGGER.info("action is restar");
					input = input.replace("-", "");

					BigDecimal nick = new BigDecimal(input);

					Double number = nick.doubleValue();

					if (!Pattern.matches(TWODEC_NUMBER, input)) {
						return "4 Mas de 2 decimales";
					}

					try {

						if (number < 0) {
							return "4 Error de sintaxis";
						}

						if (number > money) {
							throw new IllegalStateException();
						}
					} catch (NumberFormatException e) {
						return "4 Error de sintaxis";
					}

					money = money - number;
					actualiza(this, money);
					return "0 " + money;
				} else {
					LOGGER.info("action is ERROR " + input);
					return "3 Error de argumento";
				}
			}

		}
	}
}
