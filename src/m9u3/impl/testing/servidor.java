package m9u3.impl.testing;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.text.DecimalFormat;
import java.text.ParseException;

import lib.Loggers;

public class servidor {
	static final Logger LOGGER = Logger.getLogger(servidor.class.getName());

	static final Charset CHARSET = StandardCharsets.UTF_8; // it could be UTF8, ISO8859_1...
	static final int RESPONSE_TIMEOUT = 5000;
	private static final DecimalFormat df = new DecimalFormat("0.00");
	static double money = 0;

	public static void main(String[] args) {

		Loggers.configure(Level.INFO);
		server("127.0.0.1", 5508);
		// server("0.0.0.0", 5508);
	}

	static void server(String host, int port) {

		LOGGER.info("starting server at " + host + ":" + port);

		try (ServerSocket serverSocket = new ServerSocket(port, 10, InetAddress.getByName(host))) {
			serverSocket.setSoTimeout(RESPONSE_TIMEOUT);

			while (true) {
				try (Socket clientSocket = serverSocket.accept()) {

					LOGGER.info("accept from " + clientSocket.getRemoteSocketAddress());

					clientSocket.setSoTimeout(RESPONSE_TIMEOUT);
					handle(clientSocket);

				} catch (SocketTimeoutException e) {
					LOGGER.fine("timeout accepting"); // timeout in the accept: ignore
				}
			}

		} catch (IOException e) {
			LOGGER.log(Level.SEVERE, null, e);
		}
	}

	static void handle(Socket clientSocket) {

		try (PrintWriter out = new PrintWriter(new OutputStreamWriter(clientSocket.getOutputStream(), CHARSET), true);
				BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream(), CHARSET))) {

			String input;

			// null means end of stream
			while ((input = in.readLine()) != null) {
				LOGGER.info("request is " + input);
				String output = getOutput(input);
				out.println(output);
			}

			LOGGER.info("end of stream from " + clientSocket.getRemoteSocketAddress());

		} catch (SocketTimeoutException e) {
			LOGGER.fine("timeout reading"); // read timeout: do nothing

		} catch (IOException e) {
			LOGGER.log(Level.SEVERE, "handling", e);
		}
	}

	private static String getOutput(String input) {

		if (input == "") {
			return "4 Error de sintaxis";
		} else {
			if (input.charAt(0) == '+') {
				LOGGER.info("action is sumar");

				input = input.replace("+", "");

				BigDecimal nick = new BigDecimal(input);

				long number = nick.longValue();
				long decimal = Integer.parseInt(input.substring(input.indexOf('.') + 1));

				
				
				if (decimal > 99) {
					return "4. Mas de 2 decimales";
				}

				try {
					if(number == 0) {
						return "4 Error de sintaxis";
					}
					money = money + number;
				} catch (NumberFormatException e) {
					return "4 Error de sintaxis";
				}

				return "0 new saldo: " + df.format(money);

			} else if (input.charAt(0) == '-') {

				if (money <= 0) {
					return "1 No hay dinero en la cuenta";
				}

				LOGGER.info("action is restar");
				input = input.replace("-", "");

				BigDecimal nick = new BigDecimal(input);

				long number = nick.longValue();
				long decimal = Integer.parseInt(input.substring(input.indexOf('.') + 1));

				if (decimal > 99) {
					return "4. Mas de 2 decimales";
				}

				try {
					
					if(number == 0) {
						return "4 Error de sintaxis";
					}
					
					if (number > money) {
						return "1 dinero insuficiente";
					}
					money = money - number;
				} catch (NumberFormatException e) {
					return "4 Error de sintaxis";
				}

				return "0 new saldo: " + df.format(money);
			} else if (input.charAt(0) == '?') {
				LOGGER.info("action is mostrar");
				return "0 saldo = " + df.format(money);
			} else {
				LOGGER.info("action is ERROR " + input);
				return "3 Error de argumento";
			}
		}

	}
}
