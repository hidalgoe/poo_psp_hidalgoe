package m9u3.impl.testing;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.ConnectException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.logging.Level;
import java.util.logging.Logger;

import lib.Loggers;

public class cliente {
	static final Logger LOGGER = Logger.getLogger(cliente.class.getName());
	static final Charset CHARSET = StandardCharsets.UTF_8; // it could be UTF8, ISO8859_1...
	static final int CONNECT_TIMEOUT = 5000;
	static final int RESPONSE_TIMEOUT = 5000;

	public static void main(String[] args) {

	    Loggers.configure(Level.INFO);		
		client("127.0.0.1", 5508);
	    //client("10.1.5.25", 5508);
	}

	static void client(String host, int port) {
		
		
		try (Socket socket = new Socket()) {
		
			socket.connect(new InetSocketAddress(host, port), CONNECT_TIMEOUT); // connect timeout
			socket.setSoTimeout(RESPONSE_TIMEOUT); // response timeout
			
			try (PrintWriter out = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), CHARSET), true);
					BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream(), CHARSET))) {

				String comanda = "+0";
				out.println(comanda);
			    String result = in.readLine();
			    LOGGER.info("received " + result);

			}
			
		} catch (ConnectException e) {
			LOGGER.log(Level.SEVERE, "no hi ha resposta", e);
		} catch (IOException e) {
			LOGGER.log(Level.SEVERE, null, e);
		}
	}

}
