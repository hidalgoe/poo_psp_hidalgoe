package m9u3.impl.reminderb;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;
import java.util.logging.Logger;

import m9u3.base.BaseUDPServer;

public class ReminderServerImpl extends BaseUDPServer {

	static final Logger LOGGER = Logger.getLogger(ReminderServerImpl.class.getName());

	InetAddress address;
	public ReminderServerImpl(String host, int port) {

		super(host, port, 1500);
		try {
			this.address = InetAddress.getByName(host);
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	protected void handle(DatagramPacket packet) {

		byte[] b = packet.getData();
		String text = new String(b, 1, packet.getLength() - 1, StandardCharsets.UTF_8);

		byte[] nameByte = text.getBytes(StandardCharsets.UTF_8);

		DatagramPacket envio = new DatagramPacket(nameByte, nameByte.length, packet.getAddress(), packet.getPort());
		int mimir = b[0]*100;

		new Thread(() -> {

			try {
				Thread.sleep(mimir);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try {
				super.socket.send(envio);
			} catch (IOException e) { // TODO Auto-generated catch block
				e.printStackTrace();
			}

		}, "timeout").start();

	}

}
