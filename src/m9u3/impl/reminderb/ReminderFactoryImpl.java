package m9u3.impl.reminderb;

import lib.Startable;
import m9u3.prob.reminderb.ReminderClient;
import m9u3.prob.reminderb.ReminderClient.ReminderHandler;
import m9u3.prob.reminderb.ReminderFactory;

public class ReminderFactoryImpl implements ReminderFactory {

	@Override
	public ReminderClient createClient(String host, int port, ReminderHandler handler) {
		return new ReminderClientImpl(host, port, handler);
	}

	@Override
	public Startable createServer(String host, int port) {
		return new ReminderServerImpl(host, port);
	}

}
