package m9u3.impl.reminderb;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;
import m9u3.base.BaseUDPClient;
import m9u3.prob.reminderb.ReminderClient;

public class ReminderClientImpl extends BaseUDPClient implements ReminderClient {

	protected InetAddress address;
	protected int port;
	ReminderHandler handler;

	public ReminderClientImpl(String host, int port, ReminderHandler handler) {

		super(host, port, 1500);
		try {
			this.address = InetAddress.getByName(host);
			this.port = port;
			this.handler = handler;
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void submit(int tenths, String message) {

		byte[] nameByte = message.getBytes(StandardCharsets.UTF_8);
		byte[] b = new byte[nameByte.length + 1];
		b[0] = (byte) tenths;
		System.arraycopy(nameByte, 0, b, 1, nameByte.length);

		try {
			DatagramPacket packet = new DatagramPacket(b, b.length, address, port);
			super.socket.send(packet);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	@Override
	protected void handle(DatagramPacket packet) {

		byte[] b = packet.getData();
		String text = new String(b, 0, packet.getLength(), StandardCharsets.UTF_8);
		handler.onMessage(text);

	}

}
