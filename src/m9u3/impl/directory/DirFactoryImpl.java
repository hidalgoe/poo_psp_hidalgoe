package m9u3.impl.directory;

import lib.Startable;
import m9u3.prob.directory.DirClient;
import m9u3.prob.directory.DirFactory;
import m9u3.prob.directory.impl.DirServerImpl;

public class DirFactoryImpl implements DirFactory {

	@Override
	public DirClient createClient(String host, int port) {
		return new DirClientImpl(host, port);
	}

	@Override
	public Startable createServer(String host, int port) {
		return new DirServerImpl(host, port);
	}

}
