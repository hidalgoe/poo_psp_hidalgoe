package m9u3.impl.directory;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;

import m9u3.prob.directory.DirClient;
import m9u3.prob.directory.DirPeer;
import m9u3.prob.directory.LoginState;

public class DirClientImpl implements DirClient, Runnable {

	static final Logger LOGGER = Logger.getLogger(DirClientImpl.class.getName());
	static final int RESPONSE_TIMEOUT = 1500;
	static final int SIZE = 1460;

	InetAddress address;
	byte[] info;
	String host;
	int port;
	Thread thr;
	DatagramSocket socket;

	String name;
	DirClientHandler handler;
	LoginState state;

	CompletableFuture<Void> cf;

	public DirClientImpl(String host, int port) {
		this.host = host;
		this.port = port;
		thr = new Thread(this);
		state = LoginState.OUT;
		try {
			socket = new DatagramSocket();
		} catch (SocketException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void run() {

		while (true) {

			try {
				DatagramPacket packet = new DatagramPacket(new byte[SIZE], SIZE);
				socket.receive(packet);
				byte[] b = packet.getData();
				String text;
				DirPeer persona;

				switch (b[0]) {
				case 3:
					LOGGER.info("se envi� un 3");
					List<DirPeer> listPeer = new ArrayList<DirPeer>();
					if (packet.getLength() > 1) {
						text = new String(b, 1, packet.getLength() - 1, StandardCharsets.UTF_8);
						LOGGER.info("peers: " + text);
						String[] parts = text.split(",");
						for (int i = 0; i < parts.length; i++) {
							String[] pair = parts[i].split("@");
							persona = new DirPeer(pair[0], pair[1]);
							LOGGER.info(persona.getName());
							listPeer.add(persona);
						}
					}

					LOGGER.info("list is " + listPeer);
					handler.onLogin(listPeer);
					state = LoginState.IN;
					cf.complete(null);
					break;
				case 4:
					LOGGER.info("se envi� un 4");
					handler.onLogout();
					state = LoginState.OUT;
					cf.complete(null);
					break;
				case 5:
					LOGGER.info("se envi� un 5");
					text = new String(b, 1, packet.getLength() - 1, StandardCharsets.UTF_8);
					String[] pair = text.split("@");
					persona = new DirPeer(pair[0], pair[1]);
					handler.onPeerLogin(persona);
				case 6:
					LOGGER.info("se envi� un 6");
					text = new String(b, 1, packet.getLength() - 1, StandardCharsets.UTF_8);
					String[] pair2 = text.split("@");
					persona = new DirPeer(pair2[0], pair2[1]);
					handler.onPeerLogout(persona);
					break;
				case 7:
					LOGGER.info("se envi� un 7");
					handler.onStop();
					state = LoginState.OUT;
					break;
				case 8:
					LOGGER.info("se envi� un 8");
					handler.onLoginError(new IllegalArgumentException());
					state = LoginState.OUT;
					break;
				default:

				}

			} catch (SocketTimeoutException e) {
				LOGGER.fine("timeout");

			} catch (Exception e) {
				LOGGER.log(Level.SEVERE, "loop", e);
				break;
			}
		}

	}

	@Override
	public void connect() throws IOException {

		try {
			socket = new DatagramSocket();
			socket.setSoTimeout(RESPONSE_TIMEOUT);

			thr.start();

		} catch (IOException e) {
			LOGGER.log(Level.SEVERE, "sending", e);
		}

	}

	@Override
	public void disconnect() throws IOException {

		socket.close();
		thr.interrupt();
		state = LoginState.OUT;

	}

	@Override
	public void setHandler(String name, DirClientHandler handler) {

		this.name = name;
		this.handler = handler;

	}

	@Override
	public void login() {

		cf = new CompletableFuture<>();
		byte[] nameByte = name.getBytes(StandardCharsets.UTF_8);
		byte[] b = new byte[nameByte.length + 1];
		b[0] = 1;
		System.arraycopy(nameByte, 0, b, 1, nameByte.length);

		try {
			address = InetAddress.getByName(host);
			DatagramPacket packet = new DatagramPacket(b, b.length, address, port);
			socket.send(packet);
			state = LoginState.IN_REQ;
			
			new Thread(() -> {
				try {
					cf.get(RESPONSE_TIMEOUT, TimeUnit.MILLISECONDS);
				} catch (Exception e) {
					handler.onLoginError(new TimeoutException());
					state = LoginState.OUT;
				}
			}, "timeout").start();

		} catch (UnknownHostException e1) {
			e1.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		

	}

	@Override
	public void logout() {

		cf = new CompletableFuture<>();
		byte[] b = new byte[1];
		b[0] = 2;

		try {
			address = InetAddress.getByName(host);
			DatagramPacket packet = new DatagramPacket(b, b.length, address, port);
			socket.send(packet);
			state = LoginState.OUT_REQ;
			
			new Thread(() -> {
				try {
					cf.get(RESPONSE_TIMEOUT, TimeUnit.MILLISECONDS);
				} catch (Exception e) {
					handler.onLogoutError(new TimeoutException());
					state = LoginState.OUT;
				}
			}, "timeout").start();

		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public LoginState getState() {

		return state;
	}

}
