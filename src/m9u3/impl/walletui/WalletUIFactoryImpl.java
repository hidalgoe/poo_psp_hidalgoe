package m9u3.impl.walletui;

import m9u3.prob.wallet.WalletAsyncClient;
import m9u3.prob.wallet.WalletSyncClient;
import m9u3.prob.walletui.WalletPresenter;
import m9u3.prob.walletui.WalletUIFactory;
import m9u3.prob.walletui.WalletView;

public class WalletUIFactoryImpl implements WalletUIFactory {

	@Override
	public WalletPresenter createSyncPresenter(WalletView view, WalletSyncClient client) {
		return new WalletSyncPresenter(view, client);
	}

	@Override
	public WalletPresenter createAsyncPresenter(WalletView view, WalletAsyncClient client) {
		return new WalletAsyncPresenter(view, client);
	}

}
