package m9u3.impl.walletui;

import java.io.IOException;
import java.math.BigDecimal;

import m9u3.prob.wallet.WalletAsyncClient;
import m9u3.prob.wallet.WalletAsyncClient.WalletHandler;
import m9u3.prob.walletui.WalletPresenter;
import m9u3.prob.walletui.WalletView;

public class WalletAsyncPresenter implements WalletPresenter, WalletHandler {

	WalletView view;
	WalletAsyncClient client;

	public WalletAsyncPresenter(WalletView view, WalletAsyncClient client) {
		super();
		this.view = view;
		this.client = client;
		view.setListener(this);
	}

	@Override
	public void onAddAction(String amount) {

		BigDecimal bd = new BigDecimal(amount);
		client.add(bd);

	}

	@Override
	public void onTakeAction(String amount) {

		BigDecimal bd = new BigDecimal(amount);
		client.take(bd);

	}

	@Override
	public void start() {

		try {
			
			client.setHandler(this);
			client.connect();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void stop() {
		try {
			client.disconnect();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void onSuccess(BigDecimal balance) {

		view.setBalance(""+balance);
		view.showStatus("Balance modificado a: "+balance);
		
	}

	@Override
	public void onError(Throwable t) {
		
		if(t instanceof IllegalStateException) {
			view.showStatus("Error de estado");
		}
		
		if (t instanceof IllegalArgumentException){
			view.showStatus("Error de argumento");
		}
		
	}

}
