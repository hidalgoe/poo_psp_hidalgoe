package m9u3.impl.walletui;

import java.io.IOException;
import java.math.BigDecimal;

import m9u3.prob.wallet.WalletSyncClient;
import m9u3.prob.walletui.WalletPresenter;
import m9u3.prob.walletui.WalletView;

public class WalletSyncPresenter implements WalletPresenter {

	WalletView view; 
	WalletSyncClient client;
	
	public WalletSyncPresenter(WalletView view, WalletSyncClient client) {
		super();
		this.view = view;
		this.client = client;
		view.setListener(this);
	}

	@Override
	public void onAddAction(String amount) {
		
		try {
			BigDecimal bd = new BigDecimal(amount);
			bd = client.add(bd);
			amount = ""+bd;
			view.setBalance(amount);
			view.showStatus("se ha sumado: "+amount);
		} catch (IllegalArgumentException e) {
			view.showStatus("Error al sumar: "+"'"+amount+"' "+"no es valido");
		}
		
	}

	@Override
	public void onTakeAction(String amount) {
		
		try {
			BigDecimal bd = new BigDecimal(amount);
			bd = client.take(bd);
			amount = ""+bd;
			view.setBalance(amount);
			view.showStatus("se ha restado: "+amount);
		} catch (IllegalArgumentException e) {
			view.showStatus("Error al restar: "+"'"+amount+"' "+"no es valido");
		}catch (IllegalStateException e) {
			view.showStatus("Error al restar mas de la cuenta");
		}

	}

	@Override
	public void start() {
		
		try {
			client.connect();
			client.getBalance();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void stop() {
		try {
			client.disconnect();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
