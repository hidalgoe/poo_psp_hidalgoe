package m3u6.impl.ticketsdb;

import lib.db.ConnectionFactory;
import m3u6.play.jdbc.ConnectionFactories;
import m3u6.prob.ticketsdb.TicketOffice;
import m3u6.prob.ticketsdb.TicketsDB;
import m3u6.prob.ticketsdb.TicketsDBProb;

public class TicketsDBProbImpl implements TicketsDBProb {

	ConnectionFactory cf;
	

	@Override
	public ConnectionFactory getConnectionFactory() {

		return ConnectionFactories.getLoggingDataSourceFactory();

	}

	@Override
	public void initDatabase(ConnectionFactory cf, int seats) {
		
		TicketsDB.init(cf, seats);

	}

	@Override
	public TicketOffice createTicketOffice(ConnectionFactory cf)  {
		// TODO Auto-generated method stub
		return new TicketOfficeImpl(cf);
	}

}
