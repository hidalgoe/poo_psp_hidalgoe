package m3u6.impl.ticketsdb;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import lib.db.ConnectionFactory;
import lib.db.DAOException;
import lib.db.NotFoundException;
import m3u6.prob.ticketsdb.Ticket;
import m3u6.prob.ticketsdb.TicketOffice;

public class TicketOfficeImpl implements TicketOffice {

	ConnectionFactory cf;

	public TicketOfficeImpl(ConnectionFactory cf) {

		this.cf = cf;
	}

	@Override
	public int findFree() throws NotFoundException {

		try (Connection connection = cf.getConnection()) {

			String query = "SELECT * FROM tickets WHERE ocupied = 0 LIMIT 1";

			try (PreparedStatement stmt = connection.prepareStatement(query)) {

				try (ResultSet rs = stmt.executeQuery()) {

					if (rs.next()) {
						int free = rs.getInt("seat");
						return free;
					} else {
						throw new NotFoundException("not found manin");
					}

				}

			}

		} catch (SQLException e) {
			throw new DAOException("not free", e);
		}
	}

	@Override
	public Ticket buy(int seat) throws NotFoundException {

		Ticket tick = null;

		try (Connection connection = cf.getConnection()) {

			String select = "SELECT * FROM tickets WHERE seat = ?";

			try (PreparedStatement stmt = connection.prepareStatement(select)) {
				stmt.setInt(1, seat);
				try (ResultSet rs = stmt.executeQuery()) {

					if (rs.next()) {
						int seient = rs.getInt("seat");
						String Suuid = rs.getString("uuid");
						UUID uuid = UUID.fromString(Suuid);

						tick = new Ticket(seient, uuid, true);
					}
				}
			}

			String update = "UPDATE tickets SET ocupied = 1 WHERE seat = ?";

			try (PreparedStatement stmt = connection.prepareStatement(update)) {
				stmt.setInt(1, seat);
				int count = stmt.executeUpdate();

			}

		} catch (SQLException e) {
			throw new NotFoundException(e);
		}

		return tick;
	}

	@Override
	public List<Ticket> buyAll(int... seats) throws NotFoundException {

		List<Ticket> ticket = new ArrayList<Ticket>();

		try (Connection conn = cf.getConnection()) {
			conn.setAutoCommit(false);
			try {
				String select = "SELECT * FROM tickets WHERE seat = ?";
				for (int i = 0; i < seats.length; i++) {
					try (PreparedStatement stmt = conn.prepareStatement(select)) {

						stmt.setInt(1, seats[i]);

						try (ResultSet rs = stmt.executeQuery()) {
							while (rs.next()) {

								if (!rs.getBoolean("ocupied")) {
									String uuid = rs.getString("uuid");
									UUID uuidd = UUID.fromString(uuid);

									String update = "UPDATE tickets SET ocupied = 1 WHERE uuid = ?";
									try (PreparedStatement ps = conn.prepareStatement(update)) {
										ps.setString(1, uuid);
										ps.executeUpdate();

										Ticket t = new Ticket(seats[i], uuidd, true);
										ticket.add(t);
									} catch (SQLException e) {
										throw new NotFoundException("try with resources", e);

									}
								} else {
									throw new NotFoundException("paco");
								}

							}
						} catch (SQLException e) {
							throw new NotFoundException("try with resources", e);

						}

					} catch (SQLException e) {
						throw new NotFoundException("try with resources", e);

					}
				}
				conn.commit();

			} catch (Exception e) {
				conn.rollback();
				throw e;
			}

		} catch (SQLException e) {
			throw new RuntimeException("error SQL", e);
		}

		return ticket;

	}

	@Override
	public boolean isFree(int seat) {

		try (Connection connection = cf.getConnection()) {

			String select = "SELECT * FROM `tickets` WHERE seat = ?";

			try (PreparedStatement stmt = connection.prepareStatement(select)) {
				stmt.setInt(1, seat);
				try (ResultSet rs = stmt.executeQuery()) {

					if (rs.next()) {

						if (rs.getBoolean("ocupied") == true) {
							return false;
						} else {
							return true;
						}

					}
				}
			}

		} catch (SQLException e) {
		}
		return false;
	}

	@Override
	public boolean validate(Ticket ticket) {

		String query = "SELECT COUNT(*) FROM tickets WHERE seat = ? AND uuid = ? AND ocupied = ?";

		try (Connection connection = cf.getConnection()) {
			try (PreparedStatement st = connection.prepareStatement(query)) {

				if (ticket.getCode() != null) {
					st.setInt(1, ticket.getSeat());
					st.setNString(2, ticket.getCode().toString());
					st.setBoolean(3, ticket.isOcupied());

					try (ResultSet rs = st.executeQuery()) {
						rs.next();
						if (rs.getInt(1) == 1) {
							return true;
						} else
							return false;

					}
				}
			}
		} catch (SQLException e) {
			return false;
		}
		return false;
	}

	@Override
	public boolean enter(Ticket ticket) {

		String query = "SELECT * FROM tickets WHERE seat = ? AND uuid = ? AND entered = ?";

		try (Connection connection = cf.getConnection()) {
			try (PreparedStatement st = connection.prepareStatement(query)) {

				st.setInt(1, ticket.getSeat());
				st.setNString(2, ticket.getCode().toString());
				st.setBoolean(3, false);

				try (ResultSet rs = st.executeQuery()) {
					if (rs.next()) {

						String update = "UPDATE tickets SET entered = true WHERE seat = ?";

						try (PreparedStatement stmt = connection.prepareStatement(update)) {
							stmt.setInt(1, ticket.getSeat());
							int count = stmt.executeUpdate();
							return true;

						}

					} else {
						return false;
					}
				}
			}
		} catch (

		SQLException e) {
			return false;
		}

	}

	@Override
	public int size() {

		int count = 0;
		String conter = "SELECT count(*) FROM tickets";

		try (Connection connection = cf.getConnection()) {

			try (PreparedStatement stmt = connection.prepareStatement(conter)) {

				try (ResultSet rs = stmt.executeQuery()) {

					rs.next();
					return rs.getInt(1);

				}

			}

		} catch (SQLException e) {
			return 0;
		}

	}

	@Override
	public int left() {
		int count = 0;
		String conter = "SELECT count(*) FROM tickets WHERE ocupied is not null";

		try (Connection connection = cf.getConnection()) {

			try (PreparedStatement stmt = connection.prepareStatement(conter)) {

				try (ResultSet rs = stmt.executeQuery()) {

					while(rs.next()) {
						count++;
					}
					return count;

				}

			}

		} catch (SQLException e) {
			return 0;
		}
	}

}
