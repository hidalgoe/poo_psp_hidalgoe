package m3u6.impl.tasks;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import lib.db.ConnectionFactory;
import lib.db.NotFoundException;
import m3u6.prob.tasks.Task;
import m3u6.prob.tasks.TasksDAO;

public class TasksDAOImpl implements TasksDAO {

	
	ConnectionFactory cf;
	
	public TasksDAOImpl(ConnectionFactory cf){
		
		this.cf = cf;
		
	}
	
	@Override
	public Task find(Integer id) throws NotFoundException {
		String sql = "INSERT INTO taula (valor) VALUES (?)";       
		try (Connection connection = cf.getConnection(); PreparedStatement ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {                
		    ps.setInt(1, id);
		    ps.executeUpdate();
		    try (ResultSet rs = ps.getGeneratedKeys()) {
		        if (rs.next()) {
		            id = rs.getInt(1);
		        }
		    }
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void delete(Integer id) throws NotFoundException {
		// TODO Auto-generated method stub

	}

	@Override
	public Integer create(Task t) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void update(Task t) throws NotFoundException {
		// TODO Auto-generated method stub

	}

	@Override
	public List<Task> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

}
