package m3u6.impl.tasks;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import lib.db.ConnectionFactory;
import lib.db.DAOException;
import m3u6.play.jdbc.ConnectionFactories;
import m3u6.prob.tasks.Task;
import m3u6.prob.tasks.TasksProb;

public class TasksProbImpl implements TasksProb {

	ConnectionFactory cf;

	@Override
	public ConnectionFactory getConnectionFactory() throws SQLException {

		return ConnectionFactories.getDataSourceFactory();

	}

	@Override
	public List<Task> searchPending(ConnectionFactory cf) throws SQLException {

		String query = "SELECT * FROM `tasks` WHERE finished = 0";
		List<Task> pend = new ArrayList<Task>();

		try (Connection connection = cf.getConnection()) {

			Statement stmt = connection.createStatement();

			ResultSet rs = stmt.executeQuery(query);

			while (rs.next()) {

				int taskId = rs.getInt("task_id");
				String description = rs.getString("description");
				Date startDate = rs.getDate("start_date");
				Date enDate = rs.getDate("end_date");
				boolean finished = rs.getBoolean("finished");

				Task n = new Task();

				n.taskId = taskId;
				n.description = description;
				n.startDate = startDate;
				n.endDate = enDate;
				n.finished = finished;

				
				pend.add(n);

			}

			return pend;

		} catch (SQLException e) {
			throw new DAOException("try with resources", e);
		}
	}

	@Override
	public List<Task> searchByDescription(ConnectionFactory cf, String text) throws SQLException {
		int key;
		String sql = "SELECT * FROM `tasks` WHERE description LIKE ?";
		List<Task> pend = new ArrayList<Task>();
		try (Connection connection = cf.getConnection(); PreparedStatement ps = connection.prepareStatement(sql)) {
			ps.setString(1, "%" + text + "%");
			try (ResultSet rs = ps.executeQuery()) {

				while (rs.next()) {

					int taskId = rs.getInt("task_id");
					String description = rs.getString("description");
					Date startDate = rs.getDate("start_date");
					Date enDate = rs.getDate("end_date");
					boolean finished = rs.getBoolean("finished");

					Task n = new Task();

					n.taskId = taskId;
					n.description = description;
					n.startDate = startDate;
					n.endDate = enDate;
					n.finished = finished;

					pend.add(n);

				}
				return pend;
			}
		}
	}
}
