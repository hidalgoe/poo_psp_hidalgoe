package m3u6.impl.ticketsdbSolu;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import lib.db.ConnectionFactory;
import lib.db.DAOException;
import lib.db.NotFoundException;
import m3u6.prob.ticketsdb.Ticket;
import m3u6.prob.ticketsdb.TicketOffice;

public class TicketOfficeImpl implements TicketOffice {
    
    private ConnectionFactory cf;
    
    public TicketOfficeImpl(ConnectionFactory cf) {
        this.cf = cf;
    }
    
    @Override
    public boolean isFree(int seat) {
        
        try (Connection conn = cf.getConnection();
                PreparedStatement ps = conn.prepareStatement(
                        "SELECT * FROM tickets "
                        + "WHERE seat = ? AND ocupied = 0")) {
            ps.setInt(1, seat);
            try (ResultSet rs = ps.executeQuery()) {
                return rs.next();
            }

        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    @Override
    public boolean validate(Ticket ticket) {
        
        if (ticket.getCode() == null) {
            return false;
        }
        
        try (Connection conn = cf.getConnection();
                PreparedStatement ps = conn.prepareStatement(
                        "SELECT * FROM tickets "
                        + "WHERE seat = ? AND uuid = ? AND ocupied = 1")) {
            ps.setInt(1, ticket.getSeat());
            ps.setString(2, ticket.getCode().toString());
            try (ResultSet rs = ps.executeQuery()) {
                return rs.next();
            }

        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    @Override
    public int size() {

        return count("SELECT COUNT(*) FROM `tickets`");
    }

    @Override
    public int left() {
        
        return count("SELECT COUNT(*) FROM `tickets` WHERE ocupied = 0");
    }
    
    private int count(String query) {
        
        try (Connection conn = cf.getConnection();
                Statement st = conn.createStatement();
                ResultSet rs = st.executeQuery(query)) {
            if (rs.next()) {
                return rs.getInt(1);    
            }
            else {
                throw new AssertionError("impossible error!");
            }
            
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }    
    
    @Override
    public int findFree() throws NotFoundException {
        
        try (Connection conn = cf.getConnection();
                Statement st = conn.createStatement();
                ResultSet rs = st.executeQuery(
                		"SELECT seat FROM tickets "
                		+ "WHERE ocupied = 0 LIMIT 1")) {

            if (rs.next()) {
                return rs.getInt("seat");
            } else {
                throw new NotFoundException("no free seats!");
            }

        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }
    
    @Override
    public boolean enter(Ticket ticket) {
        
        try (Connection conn = cf.getConnection()) {
                                    
            try (PreparedStatement ps = conn.prepareStatement(
                    "UPDATE tickets SET entered = 1 "
                    + "WHERE seat = ? AND uuid = ? AND entered = 0")) {
                
                ps.setInt(1, ticket.getSeat());
                ps.setString(2, ticket.getCode().toString());
                
                int count = ps.executeUpdate();
                return count == 1;
            }
            
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }
    
    @Override
    public Ticket buy(int seat) throws NotFoundException {
        
        List<Ticket> result = buyAll(seat);
        return result.get(0);
    }
    
    @Override
    public List<Ticket> buyAll(int... seats) throws NotFoundException {
        
        if (seats.length == 0) {
            throw new IllegalArgumentException("seat list cannot be empty!");
        }
        
        try (Connection conn = cf.getConnection()) {
                        
            conn.setAutoCommit(false);
            
            try {                
                List<Ticket> result = findFreeSeats(conn, seats);                               
                ocupySeats(conn, seats);
                conn.commit();
                return result;
                
            } catch (Exception e) {
                conn.rollback();
                throw e;
            }
            
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }
        
	private List<Ticket> findFreeSeats(Connection conn, int[] seats)
			throws SQLException, NotFoundException {
		
		List<Ticket> result = new ArrayList<>();
		
		try (PreparedStatement ps = conn.prepareStatement(
		        "SELECT * FROM tickets WHERE ocupied = 0 AND seat IN (" 
		        		+ getWildcards(seats.length) + ")")) {
		    
		    for (int i=0; i<seats.length; i++) {
		        ps.setInt(i+1, seats[i]);
		    }
		    
		    try (ResultSet rs = ps.executeQuery()) {
		        while (rs.next()) {                            
		            int seat = rs.getInt("seat");
		            UUID uuid = UUID.fromString(rs.getString("uuid"));
		            result.add(new Ticket(seat, uuid, true));
		        }
		    }		    
		}
		
        if (result.size() < seats.length) {
            throw new NotFoundException("some seats are ocupied");
        }
		
		return result;
	}        

	private void ocupySeats(Connection conn, int[] seats) throws SQLException {
		
		try (PreparedStatement ps = conn.prepareStatement(
		        "UPDATE tickets SET ocupied = 1 WHERE seat IN (" 
		        		+ getWildcards(seats.length) + ")")) {
		    
		    for (int i=0; i<seats.length; i++) {
		        ps.setInt(i+1, seats[i]);
		    }
		    
		    int count = ps.executeUpdate();
		    if (count != seats.length) {
		        throw new AssertionError("impossible error!");
		    }
		}
	}

    private String getWildcards(int length) {
    	return "?,".repeat(length - 1) + "?";
    }
}
