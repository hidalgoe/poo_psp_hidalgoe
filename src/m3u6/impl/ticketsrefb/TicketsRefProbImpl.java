package m3u6.impl.ticketsrefb;

import lib.db.ConnectionFactory;
import m3u6.play.jdbc.ConnectionFactories;
import m3u6.prob.ticketsrefb.TicketOffice;
import m3u6.prob.ticketsrefb.TicketsRefDB;
import m3u6.prob.ticketsrefb.TicketsRefProb;

public class TicketsRefProbImpl implements TicketsRefProb {

	ConnectionFactory cf;
	
    @Override
    public ConnectionFactory getConnectionFactory() {
        return ConnectionFactories.getLoggingDataSourceFactory();
    }

    @Override
    public void initDatabase(ConnectionFactory cf, int seats) {
        TicketsRefDB.init(cf, seats);
    }

	@Override
	public TicketOffice createTicketOffice(ConnectionFactory cf, long refundMillis) {

		return new TicketOfficeImpl(cf, refundMillis);
	}


}
