package m3u6.impl.ticketsrefb;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import lib.db.ConnectionFactory;
import lib.db.DAOException;
import lib.db.NotFoundException;
import m3u6.prob.ticketsrefb.Ticket;
import m3u6.prob.ticketsrefb.TicketOffice;

public class TicketOfficeImpl implements TicketOffice {

	ConnectionFactory cf;
	Long milis;

	public TicketOfficeImpl(ConnectionFactory cf, Long milis) {

		this.cf = cf;
		this.milis = milis;
	}

	@Override
	public int findFree() throws NotFoundException {

		try (Connection connection = cf.getConnection()) {

			String query = "SELECT * FROM tickets WHERE ocupied is null LIMIT 1";

			try (PreparedStatement stmt = connection.prepareStatement(query)) {

				try (ResultSet rs = stmt.executeQuery()) {

					if (rs.next()) {
						int free = rs.getInt("seat");
						return free;
					} else {
						throw new NotFoundException("not found manin");
					}

				}

			}

		} catch (SQLException e) {
			throw new DAOException("not free", e);
		}
	}

	@Override
	public Ticket buy(int seat) throws NotFoundException {

		List<Ticket> result = buyAll(seat);
		return result.get(0);

	}

	@Override
	public List<Ticket> buyAll(int... seats) throws NotFoundException {

		if (seats.length == 0) {
			throw new IllegalArgumentException("seat list cannot be empty!");
		}

		try (Connection conn = cf.getConnection()) {

			conn.setAutoCommit(false);

			try {
				List<Ticket> result = findFreeSeats(conn, seats);
				ocupySeats(conn, seats);
				conn.commit();
				return result;

			} catch (Exception e) {
				conn.rollback();
				throw e;
			}

		} catch (SQLException e) {
			throw new DAOException(e);
		}

	}

	private List<Ticket> findFreeSeats(Connection conn, int[] seats) throws SQLException, NotFoundException {

		List<Ticket> result = new ArrayList<>();

		try (PreparedStatement ps = conn.prepareStatement(
				"SELECT * FROM tickets WHERE ocupied is null AND seat IN (" + getWildcards(seats.length) + ")")) {

			for (int i = 0; i < seats.length; i++) {
				ps.setInt(i + 1, seats[i]);
			}

			try (ResultSet rs = ps.executeQuery()) {
				while (rs.next()) {
					int seat = rs.getInt("seat");
					UUID uuid = UUID.fromString(rs.getString("uuid"));
					result.add(new Ticket(seat, uuid, System.currentTimeMillis()));
				}
			}
		}

		if (result.size() < seats.length) {
			throw new NotFoundException("some seats are ocupied");
		}

		return result;
	}

	private void ocupySeats(Connection conn, int[] seats) throws SQLException {

		try (PreparedStatement ps = conn.prepareStatement(
				"UPDATE tickets SET ocupied = 1 WHERE seat IN (" + getWildcards(seats.length) + ")")) {

			for (int i = 0; i < seats.length; i++) {
				ps.setInt(i + 1, seats[i]);
			}

			int count = ps.executeUpdate();
			if (count != seats.length) {
				throw new AssertionError("impossible error!");
			}
		}
	}

	private String getWildcards(int length) {
		return "?,".repeat(length - 1) + "?";
	}

	@Override
	public boolean isFree(int seat) {

		try (Connection connection = cf.getConnection()) {

			String select = "SELECT * FROM `tickets` WHERE seat = ?";

			try (PreparedStatement stmt = connection.prepareStatement(select)) {
				stmt.setInt(1, seat);
				try (ResultSet rs = stmt.executeQuery()) {

					if (rs.next()) {

						if (rs.getBoolean("ocupied") == true) {
							return false;
						} else {
							return true;
						}

					}
				}
			}

		} catch (SQLException e) {
		}
		return false;
	}

	@Override
	public boolean validate(Ticket ticket) {
		if (ticket.getCode() == null) {
			return false;
		}

		try (Connection conn = cf.getConnection();
				PreparedStatement ps = conn
						.prepareStatement("SELECT * FROM tickets " + "WHERE seat = ? AND uuid = ? AND ocupied is not null")) {
			ps.setInt(1, ticket.getSeat());
			ps.setString(2, ticket.getCode().toString());
			//ps.setLong(3, ticket.getOcupied());
			try (ResultSet rs = ps.executeQuery()) {
				return rs.next();
			}

		} catch (SQLException e) {
			throw new DAOException(e);
		}
	}

	@Override
	public boolean enter(Ticket ticket) {

		String query = "SELECT * FROM tickets WHERE seat = ? AND uuid = ? AND entered = ?";

		try (Connection connection = cf.getConnection()) {
			try (PreparedStatement st = connection.prepareStatement(query)) {

				st.setInt(1, ticket.getSeat());
				st.setNString(2, ticket.getCode().toString());
				st.setBoolean(3, false);

				try (ResultSet rs = st.executeQuery()) {
					if (rs.next()) {

						String update = "UPDATE tickets SET entered = true WHERE seat = ?";

						try (PreparedStatement stmt = connection.prepareStatement(update)) {
							stmt.setInt(1, ticket.getSeat());
							int count = stmt.executeUpdate();
							return true;

						}

					} else {
						return false;
					}
				}
			}
		} catch (

		SQLException e) {
			return false;
		}

	}

	@Override
	public int size() {

		int count = 0;
		String conter = "SELECT count(*) FROM tickets";

		try (Connection connection = cf.getConnection()) {

			try (PreparedStatement stmt = connection.prepareStatement(conter)) {

				try (ResultSet rs = stmt.executeQuery()) {

					rs.next();
					return rs.getInt(1);

				}

			}

		} catch (SQLException e) {
			return 0;
		}

	}

	@Override
	public int left() {
		int count = 0;
		String conter = "SELECT * FROM tickets t WHERE t.ocupied is null";

		try (Connection connection = cf.getConnection()) {

			try (PreparedStatement stmt = connection.prepareStatement(conter)) {

				try (ResultSet rs = stmt.executeQuery()) {

					while (rs.next()) {
						count++;
					}
					return count;

				}

			}

		} catch (SQLException e) {
			return 0;
		}
	}

	@Override
	public long getRefundMillis() {
		return milis;
	}

	@Override
	public boolean refund(Ticket ticket) {

		if (ticket.getOcupied() < (System.currentTimeMillis() - 500)) {
			return false;
		} else {
			try (Connection conn = cf.getConnection()) {


				try (PreparedStatement ps = conn.prepareStatement(
						"UPDATE tickets SET ocupied = null")) {

				
					int count = ps.executeUpdate();
					
				}

			} catch (SQLException e) {
			}
			return enter(ticket);
		}
	}
}
