package m3u6.impl.vtasks;

import java.sql.SQLException;

import lib.db.ConnectionFactory;
import m3u6.play.jdbc.ConnectionFactories;
import m3u6.prob.vtasks.VTasksDAO;
import m3u6.prob.vtasks.VTasksProb;

public class VTasksProbImpl implements VTasksProb {

	@Override
	public ConnectionFactory getConnectionFactory() throws SQLException {
		return ConnectionFactories.getLoggingDataSourceFactory();
	}

	@Override
	public VTasksDAO createDAO(ConnectionFactory cf) {
		
		return new VTaskDAOImpl(cf);
	}

}
