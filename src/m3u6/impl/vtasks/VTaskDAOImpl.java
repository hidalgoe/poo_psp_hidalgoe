package m3u6.impl.vtasks;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import lib.db.ConnectionFactory;
import lib.db.DAOException;
import lib.db.ModifiedException;
import lib.db.NotFoundException;
import m3u6.prob.vtasks.VTask;
import m3u6.prob.vtasks.VTasksDAO;

public class VTaskDAOImpl implements VTasksDAO {

	ConnectionFactory cf;

	public VTaskDAOImpl(ConnectionFactory cf) {

		this.cf = cf;
	}

	@Override
	public VTask find(int id) throws NotFoundException {
		try (Connection conn = cf.getConnection();
				PreparedStatement ps = conn.prepareStatement("SELECT * FROM vtasks WHERE task_id = ?")) {

			ps.setInt(1, id);
			try (ResultSet rs = ps.executeQuery()) {
				if (rs.next()) {
					VTask task = new VTask();

					task.taskId = rs.getInt("task_id");
					task.description = rs.getString("description");
					task.version = rs.getInt("version");
					task.finished = rs.getBoolean("finished");

					return task;
				} else {
					throw new NotFoundException("missing id " + id);
				}
			}

		} catch (SQLException e) {
			throw new DAOException(e);
		}
	}

	@Override
	public int create(String description) {

		String sql = "INSERT INTO vtasks (description) VALUES (?)";

		try (Connection conn = cf.getConnection();
				PreparedStatement ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {

			ps.setString(1, description);

			VTask tasca = new VTask();
			tasca.description = description;

			ps.executeUpdate();

			try (ResultSet rs = ps.getGeneratedKeys()) {
				if (rs.next()) {
					tasca.taskId = rs.getInt(1);
				} else {
					throw new DAOException("falta clau generada a tasques");
				}
			}

			return tasca.taskId;

		} catch (SQLException e) {
			throw new DAOException(e);
		}
	}

	@Override
	public void update(VTask tasca) throws NotFoundException {

		
		try (Connection conn = cf.getConnection();
				PreparedStatement ps = conn.prepareStatement("SELECT * FROM vtasks WHERE task_id = ?")) {

			conn.setAutoCommit(false);
			ps.setInt(1, tasca.taskId);
			try (ResultSet rs = ps.executeQuery()) {
				if (rs.next()) {
					VTask task = new VTask();

					task.taskId = rs.getInt("task_id");
					task.description = rs.getString("description");
					task.version = rs.getInt("version");
					task.finished = rs.getBoolean("finished");

					String sql = "UPDATE vtasks SET description = ?, finished = ?, version = ? WHERE task_id = ? AND version = ?";
					try (PreparedStatement ps2 = conn.prepareStatement(sql)) {

						ps2.setString(1, tasca.description);
						ps2.setBoolean(2, tasca.finished);
						ps2.setInt(3, tasca.version + 1);
						ps2.setInt(4, tasca.taskId);
						ps2.setInt(5, tasca.version);

						
						
						if (ps2.executeUpdate() != 1) {
							throw new ModifiedException("");
						}
						tasca.version = tasca.version + 1;
						conn.commit();

					} catch (SQLException e) {
						throw new DAOException(e);
					}
					
				} else {
					conn.rollback();
					throw new NotFoundException("missing id " + tasca.taskId);
				}
			}

		} catch (SQLException e) {
			throw new DAOException(e);
		}

	}

}
