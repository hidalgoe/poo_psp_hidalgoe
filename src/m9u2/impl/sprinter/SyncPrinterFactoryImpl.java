package m9u2.impl.sprinter;

import java.util.logging.Logger;

import m9u2.prob.sprinter.SyncPrinter;
import m9u2.prob.sprinter.SyncPrinterFactory;
import m9u2.prob.sprinter.SyncUser;

public class SyncPrinterFactoryImpl implements SyncPrinterFactory {

	static final Logger LOGGER = Logger.getLogger(SyncUser.class.getName());
	@Override
	public SyncPrinter createPrinter(long docMillis, long pageMillis) {
		
		return new  SyncPrinterImpl(docMillis, pageMillis);
		
	}
	
	public class SyncPrinterImpl implements SyncPrinter {

		long doc;
		long page;
		
		public SyncPrinterImpl(long docMillis, long pageMillis) {
			super();
			this.doc = docMillis;
			this.page = pageMillis;
		}


		@Override
		public synchronized void printDocument(String username, int pages) {
			
			LOGGER.info(username+" new job, "+pages+" pages");
			try {
				Thread.sleep(doc);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			for(int i = 1; i <= pages; i++) {
				LOGGER.info(username+ " started  page "+i);
				try {
					Thread.sleep(page);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				LOGGER.info(username+ " finished page "+i);
			}

		}

	}

}

