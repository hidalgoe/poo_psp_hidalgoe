package m9u2.impl.aprinter;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.logging.Logger;

import m9u2.play.sprinter.SyncPrinterImpl;
import m9u2.prob.aprinter.AsyncPrinter;
import m9u2.prob.aprinter.AsyncPrinterFactory;
import m9u2.prob.aprinter.Job;
import m9u2.prob.aprinter.JobListener;
import m9u2.prob.sprinter.SyncPrinter;
import m9u2.prob.sprinter.SyncUser;

public class AsyncPrinterFactoryImpl implements AsyncPrinterFactory {
	static final Logger LOGGER = Logger.getLogger(SyncUser.class.getName());

	@Override
	public AsyncPrinter createPrinter(long docMillis, long pageMillis) {

		return new AsyncPrinterImpl(docMillis, pageMillis);
	}

	public class AsyncPrinterImpl implements AsyncPrinter, Runnable {

		long doc;
		long page;
		int idJob = 1;
		boolean done = false;
		boolean sent = false;
		Thread thr;
		AsyncPrinterImpl asinc;
		
		Map<Job, JobListener> JobMap;

		BlockingQueue<Job> colaJob;
		SyncPrinter sincrona;

		@Override
		public void run() {

			while (true) {
				try {
					
					Job trabaja = colaJob.take();
					trabaja.setSent();
					sincrona.printDocument(trabaja.getUsername(), trabaja.getPages());
					
					JobListener jl = JobMap.get(trabaja);
					
					if(jl != null) {
						jl.finished(trabaja);
					}
					
					synchronized (trabaja){
						trabaja.notify();
						trabaja.setDone();
					}
					
				} catch (InterruptedException e) {
					LOGGER.info("s'ha interrumpido");
				}
			}
		}

		public AsyncPrinterImpl(long docMillis, long pageMillis) {
			super();
			this.doc = docMillis;
			this.page = pageMillis;
			colaJob = new ArrayBlockingQueue<>(100);
			sincrona = new SyncPrinterImpl(doc, page);
			thr = new Thread(this);
			
			JobMap = new HashMap<Job, JobListener>();
			thr.start();

		}

		@Override
		public Job printDocument(String username, int pages) {

			Job job = new Job(idJob, username, pages);
			idJob++;

			try {
				colaJob.put(job);
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}

			return job;
		}

		@Override
		public Job printDocument(String username, int pages, JobListener listener) {

			Job job = new Job(idJob, username, pages);
			idJob++;

			try {
				colaJob.put(job);
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}

			JobMap.put(job, listener);
			
			return job;
		}

		@Override
		public void waitForJob(Job job) {

			try {
				while(!job.isDone()) {
					synchronized(job) {
						job.wait();
					}
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			
		}

		@Override
		public void stop() {

			thr.interrupt();

		}

	}

}
