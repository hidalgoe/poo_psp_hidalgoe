package m9u2.impl.pulse;

import java.util.function.Consumer;

import m9u2.prob.pulse.Pulse;

public class PulseImpl implements Pulse {

	private static Runnable myRunnable;
	private static Thread thr;
	private volatile static boolean paused;

	public PulseImpl() {
		
	}

	static class MyRunnable implements Runnable {

		int count;
		Consumer<Integer> consumer;

		public MyRunnable(int count, Consumer<Integer> consumer) {
			this.count = count;
			this.consumer = consumer;
		}

		@Override
		public void run() {
			
			while (count >= 0) {
				if (paused) {
					try {
						Thread.sleep(10);
					} catch (InterruptedException e) {
					}
				} else {
					if (count == 0) {
						//count--;
						consumer.accept(count);
						return;
					} else {
						try {
							consumer.accept(count);
							count--;
							Thread.sleep(1000);
						} catch (InterruptedException e) {
						}
					}
				}
			}
		}
	}

	@Override
	public void begin(int count, Consumer<Integer> consumer) {
		myRunnable = new MyRunnable(count, consumer);
		thr = new Thread(myRunnable);
		thr.start();
	}

	@Override
	public void hold() {
		try {
			thr.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void pause() {
		paused = true;
	}

	@Override
	public void unpause() {
		paused = false;
	}

}
