package m9u2.impl.timer1;

import m9u2.prob.timer1.Timer1;

public class Timer1Impl implements Timer1 {

	private static long milis;
	private static Thread thread;
	Runnable myRunnable1 = new MyRunnable1();

	public Timer1Impl(long milis) {

		Timer1Impl.milis = milis;
		thread = new Thread(myRunnable1);

	}

	static class MyRunnable1 implements Runnable {

		@Override
		public void run() {
			try {
				Thread.sleep(milis);
			} catch (InterruptedException e) {
				return;
			}

		}
	}

	@Override
	public long millis() {
		return milis;
	}

	public void go() {
		
		thread.start();
				
	}

	@Override
	public void cancel() {
		thread.interrupt();

	}

	@Override
	public void hold() {
		try {
			thread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean done() {

		if (thread.isAlive()) {
			return false;
		} else {
			return true;
		}
	}

	@Override
	public Thread getThread() {
		return thread;
	}

}
