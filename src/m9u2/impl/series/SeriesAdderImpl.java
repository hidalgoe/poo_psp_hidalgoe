package m9u2.impl.series;

import m9u2.prob.series.Adders.LongBiAdder;

import java.util.logging.Logger;

import javafx.scene.chart.XYChart.Series;
import m9u2.prob.series.SeriesAdder;

public class SeriesAdderImpl implements SeriesAdder {

	static final Logger LOGGER = Logger.getLogger(Series.class.getName());

	private static Runnable myRunnable;
	private static volatile int valorFinal;
	private static long[] arrayx;

	@Override
	public long add(LongBiAdder adder, int limit, int parts) {

		int x = limit;
		int i = parts;

		long inter = 0;

		long[] array = new long[i];
		long[] array2 = new long[i];
		arrayx = new long[i];

		int xa = 0;

		for (int e = 1; e <= x; e++) {

			if (xa == i) {
				xa = 0;
			}

			array[xa]++;

			xa++;
		}

		long lst = 0;
		for (int j = 0; j < array.length; j++) {

			if (array[j] >= lst) {
				lst = array[j];
				array2[j]++;
			} else {
				array2[j] = 0;
			}

		}
		/////////////////////////////////////////////////////////////////

		Thread myThreads[] = new Thread[i];
		long to = 0;
		for (int e = 0; e < i; e++) {

			to = array[e] + to;

			if (array2[e] == 1) {
				inter++;
			} else {
				inter++;
			}

			myRunnable = new MyRunnable(e, inter, to, adder);
			myThreads[e] = new Thread(myRunnable);
			myThreads[e].start();

			inter = to;
		}
		
		for (int e = 0; e < i; e++) {
			try {
				myThreads[e].join();
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}

		/////////////////////////////////////////////////////////////////

		long valorFinal = 0;

		for (int s = 0; s < arrayx.length; s++) {
			valorFinal = adder.add(valorFinal, arrayx[s]);
		}

		return valorFinal;
	}

	static class MyRunnable implements Runnable {

		long to;
		LongBiAdder adder;
		long thenumba;
		int n;
		long inter;

		public MyRunnable(int n, long inter, long to, LongBiAdder adder) {
			this.to = to;
			this.adder = adder;
			this.n = n;
			this.inter = inter;
		}

		@Override
		public void run() {

			for (long i = inter; i <= to; i++) {

				thenumba = adder.add(thenumba, i);
			}

			LOGGER.info("Thread: " + n + " finish");
			arrayx[n] = thenumba;

		}
	}

}
