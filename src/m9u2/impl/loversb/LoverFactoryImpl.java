package m9u2.impl.loversb;

import java.util.logging.Logger;

import m9u2.prob.loversb.Lover;
import m9u2.prob.loversb.LoverFactory;
import m9u2.prob.sprinter.SyncUser;

public class LoverFactoryImpl implements LoverFactory {

	static final Logger LOGGER = Logger.getLogger(SyncUser.class.getName());

	@Override
	public Lover createLover(String name) {
		return new LoverImpl(name);
	}

	public class LoverImpl implements Lover, Runnable {

		String name;
		Thread thr;
		boolean vivo;

		Lover amante;

		public LoverImpl(String name) {
			super();
			this.name = name;
			this.vivo = true;
			thr = new Thread(this);

			thr.start();
		}

		@Override
		public void run() {

			synchronized (thr) {
				while (vivo) {

					try {
						thr.wait();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}

			}

		}

		@Override
		public String getName() {
			return name;
		}

		@Override
		public Lover getLover() {

			return amante;
		}

		@Override
		public void surrender() {
			this.vivo = false;
			thr.interrupt();
		}

		@Override
		public void hold() {
			
			try {
				thr.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
		}

		@Override
		public boolean loveFrom(Lover wooer) {

			if (vivo == true && amante == null) {

				this.amante = wooer;
				wooer.loveFrom(this);

				LOGGER.info(amante.getName());

				synchronized (thr) {
					this.vivo = false;
					thr.notify();
				}

				return true;

			} else {
				return false;
			}

		}

	}

}
