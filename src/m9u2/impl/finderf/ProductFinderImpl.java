package m9u2.impl.finderf;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.logging.Logger;

import lib.ThreadResult;
import m9u2.prob.finder.Product;
import m9u2.prob.finder.ProductFinder;
import m9u2.prob.finder.Shipment;

public class ProductFinderImpl implements ProductFinder{
	
	static final Logger LOGGER = Logger.getLogger(ProductFinderImpl.class.getName());
	
	long milis;
	private static Runnable myRunnable;
	Thread myThreads = new Thread();
	
	
	public ProductFinderImpl(long milis) {
		super();
		this.milis = milis;
	}

	@Override
	public Shipment createShipment(int productCount) {

		ShipmentImpl ship = new ShipmentImpl(productCount);
		return ship;
	}

	@Override
	public Product find(Shipment prods, int serial, int numWorkers) {		
		
		//ThreadResult<Product> tr = new ThreadResult<Product>();
		CompletableFuture<Product> cf = new CompletableFuture<>();
		
		Product p = new Product(serial);
		
		for (int i = 0; i < numWorkers; i++) {
			
			myRunnable = new MyRunnable(cf, prods, p, serial);
			myThreads = new Thread(myRunnable);
			myThreads.start();
		}
		
		LOGGER.info("waiting...");
		
			try {
				p =  cf.get();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		
		return p;
		
	}
	
	static class MyRunnable implements Runnable {

		CompletableFuture<Product> tr;
		Product p;
		int serial;
		Shipment prods;
		
		public MyRunnable(CompletableFuture<Product> tr, Shipment prods,Product p, int serial) {
			this.tr = tr;
			this.prods = prods;
			this.p = p;
			this.serial = serial;
		}

		@Override
		public void run() {
			
			int i = 0;
			while(prods.next() != null && tr.isDone()) {
				i++;
			}
			
			LOGGER.info("found S/N:"+ p.getSerial() +" after "+i+" items");
			tr.complete(p);
			
		}
	}
	
	
	public class ShipmentImpl implements Shipment{

		List<Product> lista = new ArrayList<Product>();
		int i = 1;
		int n = 0;
		
		public ShipmentImpl(int n) {
		
			this.n = n;
			
			for(int i = 1; i <= n; i++) {
				Product prod = new Product(i);
				lista.add(prod);
			}		
		}
		
		@Override
		public synchronized Product next() {		
			
			if(i > n) {
				return null;
			}
			
			Product x = lista.get(i-1);
			i++;
			
			return x;
		}
		
	}
}
