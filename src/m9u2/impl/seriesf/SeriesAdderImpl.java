package m9u2.impl.seriesf;

import m9u2.prob.series.Adders.LongBiAdder;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.logging.Logger;
import javafx.scene.chart.XYChart.Series;
import m9u2.prob.series.SeriesAdder;

public class SeriesAdderImpl implements SeriesAdder {

	static final Logger LOGGER = Logger.getLogger(Series.class.getName());

	// private static Runnable myRunnable;
	private static volatile int valorFinal;
	// private static long[] arrayx;

	@Override
	public long add(LongBiAdder adder, int limit, int parts) {

		int x = limit;
		int i = parts;
		long valorFinal = 0;

		long inter = 0;

		long[] array = new long[i];
		long[] array2 = new long[i];
		// arrayx = new long[i];

		int xa = 0;

		for (int e = 1; e <= x; e++) {

			if (xa == i) {
				xa = 0;
			}

			array[xa]++;

			xa++;
		}

		long lst = 0;
		for (int j = 0; j < array.length; j++) {

			if (array[j] >= lst) {
				lst = array[j];
				array2[j]++;
			} else {
				array2[j] = 0;
			}

		}

		// -------------------------------------

		ExecutorService executor = Executors.newFixedThreadPool(1000);

		// -------------------------------------

		/////////////////////////////////////////////////////////////////

		@SuppressWarnings("unchecked")
		Future<Long>[] futures = new Future[i];
		long to = 0;
		for (int e = 0; e < i; e++) {

			to = array[e] + to;

			if (array2[e] == 1) {
				inter++;
			} else {
				inter++;
			}

			futures[e] = executor.submit(new Numbers(e, inter, to, adder));

			inter = to;

		}

		for (int e = 0; e < i; e++) {
			try {
				valorFinal = adder.add(valorFinal, futures[e].get());
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			} catch (ExecutionException e1) {
				e1.printStackTrace();
			}
		}
		/////////////////////////////////////////////////////////////////

		return valorFinal;
	}

	private static final class Numbers implements Callable<Long> {

		long to;
		LongBiAdder adder;
		long thenumba;
		int n;
		long inter;

		public Numbers(int n, long inter, long to, LongBiAdder adder) {
			this.to = to;
			this.adder = adder;
			this.n = n;
			this.inter = inter;
		}

		@Override
		public Long call() throws Exception {
			for (long i = inter; i <= to; i++) {

				thenumba = adder.add(thenumba, i);
			}

			LOGGER.info("Thread: " + n + " finish");
			// arrayx[n] = thenumba;

			return thenumba;
		}
	}

}
