package m3u5.impl.memevents;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import m3u5.prob.memevents.MemGame;
import m3u5.prob.memevents.MemGameListener;
import m3u5.prob.memory.MemCard;
import m3u5.prob.memory.MemGameState;

public class MemGameImpl implements MemGame {

	private MemGameState state;

	private Map<Integer, String> map;
	private Map<Integer, Integer> points;

	LinkedList<MemGameListener> list;

	private List<MemCard> cards;

	private int playerONE = 0;
	private int playerTWO = 0;
	private int pONE = 0;
	private int pTWO = 0;

	private int playerId = 0;
	private int p = 0;

	private int totalcards;
	private int turn = 0;
	private int pos1 = 0;
	private int pos2 = 0;
	private String card1 = null;
	private String card2 = null;

	public MemGameImpl() {
		state = state.INIT;
		map = new HashMap<Integer, String>();
		points = new HashMap<Integer, Integer>();
		cards = new ArrayList<MemCard>();
		list = new LinkedList<MemGameListener>();

	}

	@Override
	public void addListener(MemGameListener listener) {

		list.add(listener);

	}

	@Override
	public int join(String name) {

		if (turn == 0) {
			playerONE = playerId;
			pONE = 0;
		} else {
			playerTWO = playerId;
			pTWO = 0;
		}

		int n = playerId;
		map.put(n, name);
		list.forEach(ear -> ear.onJoin(n, name));
		points.put(n, 0);
		playerId++;

		return n;

	}

	@Override
	public void start(List<String> values) {

		totalcards = values.size();

		list.forEach(ear -> ear.onTurn(0, true));

		for (int i = 0; i < points.size(); i++) {
			final int x = i;
			list.forEach(ear -> ear.onScore(x, 0));
		}

		for (int i = 0; i < values.size(); i++) {
			MemCard mc = new MemCard(values.get(i), false);
			cards.add(mc);
		}

		state = MemGameState.LEFT_2;
		list.forEach(ear -> ear.onState(state.LEFT_2));

	}

	@Override
	public void play(int player, int pos) {

		if (state == MemGameState.LEFT_2) {

			cards.set(pos, new MemCard(cards.get(pos).value(), true));
			card1 = cards.get(pos).value();
			state = MemGameState.LEFT_1;
			list.forEach(ear -> ear.onState(state.LEFT_1));
			pos1 = pos;
			list.forEach(ear -> ear.onReveal(pos1, card1));
		} else if (state == MemGameState.LEFT_1) {

			cards.set(pos, new MemCard(cards.get(pos).value(), true));
			card2 = cards.get(pos).value();
			pos2 = pos;
			list.forEach(ear -> ear.onReveal(pos2, card2));
			
			if (card1.equals(card2)) {

				points.remove(player);
				points.put(player, p++);

				if (turn == 0) {
					pONE = p;
					list.forEach(ear -> ear.onScore(0, pONE));
				} 
				
				if (turn == 1) {
					pTWO = p;
					list.forEach(ear -> ear.onScore(1, pTWO));
				}
				
				
				boolean quedan = false;

				for (MemCard mc : cards) {
					if (mc.revealed() == false) {
						quedan = true;
					}
				}

				if (quedan) {
					state = MemGameState.LEFT_2;
					list.forEach(ear -> ear.onState(state.LEFT_2));
				} else {
					state = MemGameState.FINISHED;
					list.forEach(ear -> ear.onState(state.FINISHED));
				}

			} else {
				
				if (turn == 0) {
					turn = 1;
					list.forEach(ear -> ear.onTurn(0, false));
					list.forEach(ear -> ear.onTurn(1, true));
				} else {
					turn = 0;
					list.forEach(ear -> ear.onTurn(1, false));
					list.forEach(ear -> ear.onTurn(0, true));
				}
				
				state = MemGameState.LEFT_0;
				list.forEach(ear -> ear.onState(state.LEFT_0));
				Timer timer = new Timer();
				timer.schedule(new TimerTask() {
					@Override
					public void run() {
						cards.set(pos1, new MemCard(card1, false));
						cards.set(pos2, new MemCard(card2, false));
						state = MemGameState.LEFT_2;
						list.forEach(ear -> ear.onState(state.LEFT_2));

						
						
						list.forEach(ear -> ear.onUnreveal(pos1));
						list.forEach(ear -> ear.onUnreveal(pos2));

					}
				}, 1500);

			}
		}

	}

	@Override
	public MemCard getCard(int idx) {

		return cards.get(idx);

	}

	@Override
	public String getPlayer(int idx) {
		return map.get(idx);
	}

	@Override
	public int getScore(int player) {
		return points.get(player);
	}

	@Override
	public MemGameState getState() {
		return state;
	}

	@Override
	public int getTurn() {
		return turn;
	}

	@Override
	public int getPlayersCount() {
		return playerId;
	}

	@Override
	public int getCardsCount() {
		return totalcards;
	}

	@Override
	public void stop() {
		// TODO Auto-generated method stub

	}

}
