package m3u5.impl.shoplistalt;

import java.util.ArrayList;
import java.util.Iterator;

import m3u5.impl.shoplist.ShopProductImpl;
import m3u5.prob.shoplist.ShopList;
import m3u5.prob.shoplist.ShopProduct;

public class ShopListImpl implements ShopList {

	ArrayList<ShopProduct> productes;
	ShopProduct shp;


	public ShopListImpl() {

		productes = new ArrayList<ShopProduct>();

	}

	@Override
	public Iterator<ShopProduct> iterator() {
		Iterator it = productes.iterator();
		return it;

	}

	@Override
	public int size() {

		return productes.size();
	}

	@Override
	public ShopProduct findProduct(String name) {
		boolean is = false;
		int pos = 0;

		for (int i = 0; i < productes.size(); i++) {

			if (productes.get(i).getName().equalsIgnoreCase(name)) {
				pos = i;
				is = true;
			}

		}

		if (is) {
			return productes.get(pos);

		} else {
			return null;
		}

	}

	@Override
	public boolean addProduct(ShopProduct p) {

		if (productes.contains(p)) {
			return false;
		} else {
			productes.add(p);
			return true;
		}

	}

	@Override
	public boolean removeProduct(String name) {
		
		boolean is = false;
		int pos = 0;

		for (int i = 0; i < productes.size(); i++) {

			if (productes.get(i).getName().equalsIgnoreCase(name)) {
				is = true;
				productes.remove(i);
			}

		}

			return is;
		
	}

	@Override
	public int addUnits(String name, int units) {
		for (int i = 0; i < productes.size(); i++) {
			if (productes.get(i).getName().equals(name)) {
				int sum = productes.get(i).getUnits()+units;
				shp = new ShopProductImpl(name, productes.get(i).getUnitPrice(), sum);
				removeProduct(name);
				productes.add(shp);
				return shp.getUnits();
			}
		}
		return -1;
	}

	@Override
	public int removeUnits(String name, int units) {
		for (int i = 0; i < productes.size(); i++) {
			if (productes.get(i).getName().equals(name) && 
					productes.get(i).getUnits() >= units) {
				int res = productes.get(i).getUnits()-units;
				shp = new ShopProductImpl(name, productes.get(i).getUnitPrice(), res);
				removeProduct(name);
				productes.add(shp);
				return shp.getUnits();
			}
		}
		return -1;
	}

	@Override
	public boolean setUnitPrice(String name, double price) {
		for (int i = 0; i < productes.size(); i++) {
			if (productes.get(i).getName().equalsIgnoreCase(name)) {
				shp = new ShopProductImpl(name, price, productes.get(i).getUnits());
				productes.add(shp);
				return true;
			}
		}
		return false;
	}

}
