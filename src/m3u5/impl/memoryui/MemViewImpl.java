package m3u5.impl.memoryui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import m3u5.prob.memoryui.MemUI.MemView;
import m3u5.prob.memoryui.MemUI.MemViewListener;

public class MemViewImpl implements MemView {

	static final int WIDTH = 750;
	static final int HEIGHT = 400;

	private Label turnLbl, scorelbl, namesLbl;

	Button btn;
	int counter = 0;
	private Stage stage;
	private MemViewListener listen;
	Button overBtn;
	Map<Integer, Integer> scores = new HashMap<Integer, Integer>();

	List<Button> cards = new ArrayList<Button>();
	List<String> players = new ArrayList<String>();;

	public MemViewImpl(Stage stage) {

		this.stage = stage;
		this.stage.setScene(createNamesScene());

	}

	@Override
	public void setListener(MemViewListener l) {
		this.listen = l;
	}

	private Scene createNamesScene() {

		VBox root = new VBox(10);
		root.setAlignment(Pos.CENTER);
		root.setStyle("-fx-font-size: 24pt");

		Label msgLbl = new Label("Enter player names");

		HBox namesBox = new HBox(20);
		namesBox.setAlignment(Pos.CENTER);

		HBox inputBox = new HBox(20);
		inputBox.setAlignment(Pos.CENTER);
		TextField nameTxt = new TextField();
		Button joinBtn = new Button("Join");
		namesLbl = new Label();
		namesBox.getChildren().addAll(new Label("Joined:"), namesLbl);
		joinBtn.setOnAction(event -> listen.onJoinAction(nameTxt.getText(), true));

		inputBox.getChildren().addAll(new Label("Name:"), nameTxt, joinBtn);

		Button startBtn = new Button("Start");
		startBtn.setOnAction(event -> listen.onStartAction());

		root.getChildren().addAll(msgLbl, namesBox, inputBox, startBtn);
		return new Scene(root, WIDTH, HEIGHT);
	}

	private Scene createCardsScene(int count, List<String> names) {

		VBox root = new VBox(10);
		root.setAlignment(Pos.CENTER);
		root.setStyle("-fx-font-size: 24pt"); // default font size

		HBox scoresPane = new HBox(20);
		scoresPane.setAlignment(Pos.CENTER);

		for (int i = 0; i < names.size(); i++) {
			scorelbl = new Label("" + scores);
			scoresPane.getChildren().addAll(new Label(names.get(i)), scorelbl);
		}

		HBox counter = new HBox(20);
		counter.setAlignment(Pos.CENTER);
		turnLbl = new Label();
		counter.getChildren().addAll(new Label("Turno del jugador: "), turnLbl);

		FlowPane cardsPane = new FlowPane(2, 2);
		cardsPane.setAlignment(Pos.CENTER);

		cardsPane.getChildren().clear();
		for (int i = 0; i < count; i++) {
			btn = new Button("?");
			cards.add(i, btn);
			btn.setOpacity(0.5); // grayed when not revealed
			cardsPane.getChildren().add(btn);
			final int pos = i;
			btn.setOnAction(event -> listen.onCardAction(pos));
		}

		Label statusLbl = new Label();
		overBtn = new Button("Restart");
		overBtn.setOnAction(event -> listen.onRestartAction());
		overBtn.setVisible(false);

		root.getChildren().addAll(scoresPane, statusLbl, counter, cardsPane, overBtn);
		return new Scene(root, WIDTH, HEIGHT);
	}

	@Override
	public void showStartView() {
		stage.setScene(createNamesScene());
		this.stage.show();
	}

	@Override
	public void showBoardView(int cardCount, List<String> names) {

		stage.setScene(createCardsScene(cardCount, names));

	}

	@Override
	public void showJoin(String name) {

		namesLbl.setText(namesLbl.getText() + " " + name);
		players.add(name);

	}

	@Override
	public void resetBoard() {

		for (int i = 0; i < scores.size(); i++) {
			scores.put(i, 0);
		}

		for (int i = 0; i < cards.size(); i++) {
			cards.get(i).setText("?");
			overBtn.setVisible(false);
		}
	}

	@Override
	public void askBoardRestart(boolean enable) {

	}

	@Override
	public void setTurn(int pidx) {

		turnLbl.setText(players.get(pidx));

	}

	@Override
	public void unsetTurn(int pidx) {
		turnLbl.setText(players.get(pidx));

	}

	@Override
	public void setScore(int pidx, int score) {

		scores.put(pidx, score);

		if (score == (cards.size() / 2)) {
			overBtn.setVisible(true);
		}

	}

	@Override
	public void showCard(int position, String value) {

		cards.get(position).setText(value);

	}

	@Override
	public void hideCard(int position) {

		Platform.runLater(() -> {
			cards.get(position).setText("?");
		});

	}

	@Override
	public void setStatus(String text) {
		// TODO Auto-generated method stub

	}

}
