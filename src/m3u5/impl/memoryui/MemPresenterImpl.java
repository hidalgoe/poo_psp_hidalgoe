package m3u5.impl.memoryui;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;
import java.util.logging.Level;
import java.util.logging.Logger;

import m3u5.prob.memevents.MemGame;
import m3u5.prob.memory.MemGameState;
import m3u5.prob.memoryui.MemUI.MemPresenter;
import m3u5.prob.memoryui.MemUI.MemView;

public class MemPresenterImpl implements MemPresenter {

    static final Logger LOGGER = Logger.getLogger(MemPresenterImpl.class.getName());
    
    private MemView view; 
    private MemGame game;
    
    private List<Player> players;
    private Map<Integer, Integer> idToIdx;
    private Supplier<List<String>> supplier;
    
    public MemPresenterImpl(MemView view, MemGame game) {
    
        this.idToIdx = new HashMap<>();
        this.players = new ArrayList<>();
        
        this.view = view;
        this.game = game;
        
        view.setListener(this);
        game.addListener(this);        
    }
    
    @Override
    public void start(Supplier<List<String>> supplier) { 
        
        this.supplier = supplier;
        view.showStartView();
    }
        
    @Override
    public void start(final int count) {
        
        this.supplier = () -> {            
            List<String> values = new ArrayList<>();
            for (int i=0; i<count; i++) {
                String chstr = Character.toString(i + 65);
                values.add(chstr);
                values.add(chstr);
            }
            Collections.shuffle(values);
            return values;
        };
        view.showStartView();
    }
    
    @Override
    public void stop() {        
        
        game.stop();
    }

    // view listener
    
    @Override
    public void onCardAction(int position) {
        
        LOGGER.info("onCardAction " + position);
        
        try {
            game.play(game.getTurn(), position);
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, "while playing", e);
        }
    }
    
    @Override
    public void onJoinAction(String name, boolean interactive) {
                
        LOGGER.info("onJoinAction " + name);
        
        int id = game.join(name);
        idToIdx.put(id, players.size());
        players.add(new Player(id, name));    
    }
        
	@Override
	public void onStartAction() {

	    LOGGER.info("onStartAction");
	    
	    List<String> names = new ArrayList<>();
        for (Player p: players) {
            names.add(p.name);
        }
	    
        List<String> values = supplier.get();
        view.showBoardView(values.size(), names);        
        game.start(values);
	}
	
	@Override
	public void onRestartAction() {
	    
	    LOGGER.info("onRestartAction");
	    
	    view.askBoardRestart(false);
	    view.resetBoard();
	    game.start(supplier.get());
	}
    
    // model listener
    
    @Override
    public void onTurn(int player, boolean set) {
        
        LOGGER.info("onTurn " + player + " " + set);
        
        int pidx = idToIdx.get(player);
        
        if (set) {
            view.setTurn(pidx);
        }
        else {
            view.unsetTurn(pidx);
        }
    }

    @Override
    public void onScore(int player, int score) {
        
        LOGGER.info("onScore " + player + " " + score);
        
        int pidx = idToIdx.get(player);
        view.setScore(pidx, score);
    }

    @Override
    public void onReveal(int pos, String value) {
        
        LOGGER.info("onReveal " + pos + " " + value);
        
        view.showCard(pos, value);
    }

    @Override
    public void onUnreveal(int pos) {
        
        LOGGER.info("onUnreveal " + pos);
        
        view.hideCard(pos);
    }

    @Override
    public void onState(MemGameState state) {
        
        LOGGER.info("onState " + state);
        
        if (state == MemGameState.FINISHED) {
            int winner = calculateWinner();
            if (winner == -1) {
                view.setStatus("draw! (" + getTotalsString() + ")");
            }
            else {
                view.setStatus("winner is " + game.getPlayer(winner) 
                    + "! (" + getTotalsString() + ")");
            }
        	view.askBoardRestart(true);
        }
    }

    @Override
    public void onJoin(int player, String name) {
        
        LOGGER.info("onJoin " + player + " " + name);
        
        view.showJoin(name);        
    }
    
    // WINNER
    
    private String getTotalsString() {
        
        StringBuilder sb = new StringBuilder();
        for (Player p: players) {
            if (sb.length() > 0) {
                sb.append(":");
            }
            sb.append(p.total);
        }
        return sb.toString();
    }

    private int calculateWinner() {
        
        Player winner = null;
        int topScore = 0;
        
        for (Player p: players) {
            int score = game.getScore(p.id);
            if (score > topScore) {
                winner = p;
                topScore = score;
            }
        }
        
        int count = 0;
        for (Player p: players) {
            int score = game.getScore(p.id);
            if (score == topScore) {
                count ++;
            }
        }
        
        if (winner != null && count == 1) {
            winner.total += 1;
            return winner.id;
        }
        else {
            return -1;
        }
    }

    // PLAYERS

    private static class Player {
        
        int id, total;
        String name;
        Player(int id, String name) {
            this.id = id;
            this.name = name;
        }
    }    
}