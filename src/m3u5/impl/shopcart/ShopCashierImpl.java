package m3u5.impl.shopcart;

import java.util.LinkedList;
import java.util.NoSuchElementException;
import java.util.Queue;

import m3u5.prob.shopcart.PaymentException;
import m3u5.prob.shopcart.ShopCart;
import m3u5.prob.shopcart.ShopCashier;

public class ShopCashierImpl implements ShopCashier {

	private Queue<ShopCart> listaCart = new LinkedList<ShopCart>();
	private int fails;

	public ShopCashierImpl() {
		listaCart = new LinkedList<ShopCart>();
	}

	@Override
	public int size() {
		return listaCart.size();
	}

	@Override
	public void queue(ShopCart cart) {
		if (cart == null) {
			throw new IllegalArgumentException("aquest carro es null");

		}
		listaCart.add(cart);
	}

	@Override
	public void serve() {

		if (listaCart.isEmpty()) {
			throw new NoSuchElementException();
		} else {
			ShopCart sc = listaCart.remove();

			try {
				sc.pay();

			} catch (PaymentException e) {
				fails++;
			}

		}
	}

	@Override
	public int failures() {
		return fails;
	}

}
