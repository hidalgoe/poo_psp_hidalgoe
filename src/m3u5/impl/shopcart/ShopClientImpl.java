package m3u5.impl.shopcart;

import m3u5.prob.shopcart.NotEnoughCreditException;
import m3u5.prob.shopcart.ShopClient;

public class ShopClientImpl implements ShopClient {

	private double credit;
	

	public ShopClientImpl(double credit) {
		super();
		this.credit = credit;
	}

	@Override
	public void takeCredit(double amount) throws NotEnoughCreditException {
		
		if(credit < amount) {
			throw new NotEnoughCreditException();
		}else {
			this.credit = this.credit-amount;
		}
	}

	@Override
	public double getCredit() {
		return credit;
	}

	@Override
	public void addCredit(double amount) {
		credit = credit + amount;
	}

}
