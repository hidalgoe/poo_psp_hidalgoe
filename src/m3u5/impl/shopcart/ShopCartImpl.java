package m3u5.impl.shopcart;

import m3u5.impl.shoplist.ShopListImpl;
import m3u5.prob.shopcart.NotEnoughCreditException;
import m3u5.prob.shopcart.PaymentException;
import m3u5.prob.shopcart.ShopCart;
import m3u5.prob.shopcart.ShopClient;
import m3u5.prob.shoplist.ShopList;
import m3u5.prob.shoplist.ShopProduct;

public class ShopCartImpl implements ShopCart {

	private ShopList lista;
	private ShopClient clnt;
	private boolean paid;

	public ShopCartImpl(ShopClient cliente) {

		this.clnt = cliente;
		lista = new ShopListImpl();

	}

	@Override
	public ShopList getList() {
		return lista;
	}

	@Override
	public ShopClient getClient() {
		return clnt;
	}

	@Override
	public void pay() {

		double total = 0;

		for (ShopProduct sp : lista) {

			total = total+(sp.getUnits() * sp.getUnitPrice());

		}
		
		if(paid) {
			throw new PaymentException();
		}
		
		try {
			clnt.takeCredit(total);
			paid = true;
		} catch (NotEnoughCreditException e) {
			throw new PaymentException();
		}

	}

	@Override
	public boolean isPaid() {
		return paid;
	}

}
