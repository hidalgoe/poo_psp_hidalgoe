package m3u5.impl.shopcart;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import lib.Loggers;
import m3u5.prob.shopcart.NotEnoughCreditException;

public class ShopCartClientTest {
	protected ShopClientImpl prob;
	int init = 10;

	@BeforeAll
	static void beforeAll() {
		Loggers.configure();
	}

	@BeforeEach
	void beforeEach() {
		prob = new ShopClientImpl(init);
	}

	@Test
	void testInitialCredit() {
		assertTrue(prob.getCredit() == init);
	}

	@Test
	void testAddCredit() {
		prob.addCredit(5);
		assertFalse(prob.getCredit() == init);
		assertTrue(prob.getCredit() == 15);
	}

	@Test
	void testJustCredit() throws NotEnoughCreditException {
		prob.takeCredit(10);
		assertTrue(prob.getCredit() == 0);
		assertFalse(prob.getCredit() > 0);
	}

	@Test
	void testSameCredit() throws NotEnoughCreditException {
		prob.takeCredit(0);

		assertTrue(prob.getCredit() == init);

	}

	@Test
	void testEnoughCredit() throws NotEnoughCreditException {
		prob.takeCredit(5);

		assertTrue(prob.getCredit() == init - 5);

	}

	@Test
	void testNotEnoughCredit() {
		try {
			prob.takeCredit(init + 5);
			fail("no hay suficiente credito");
		} catch (NotEnoughCreditException e) {
		}
	}

}
