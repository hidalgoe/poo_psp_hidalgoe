package m3u5.impl.cards;

import java.util.Objects;

import m3u5.prob.cards.Card;

public class CardImpl <S,F> implements Card<S, F> {

	private S s;
	private F f;
	
	@Override
	public S getSuit() {
		return s;
	}

	@Override
	public F getFigure() {
		return f;
	}

	public CardImpl(S s, F f) {
		super();
		this.s = s;
		this.f = f;
	}

	@Override
	public int hashCode() {
		return Objects.hash(f, s);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		@SuppressWarnings("unchecked")
		CardImpl<S, F> other = (CardImpl<S, F>) obj;
		return Objects.equals(f, other.f) && Objects.equals(s, other.s);
	}

	@Override
	public String toString() {
		return "CardImpl [s=" + s + ", f=" + f + "]";
	}
	
	

}
