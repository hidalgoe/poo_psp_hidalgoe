package m3u5.impl.cards;

import java.util.Collections;
import java.util.LinkedList;
import java.util.NoSuchElementException;

import m3u5.prob.cards.Card;
import m3u5.prob.cards.CardPile;

public class CardPileImpl<S, F> implements CardPile<S, F> {

	private LinkedList<Card<S, F>> pila;

	public CardPileImpl() {
		super();
		pila = new LinkedList<Card<S, F>>();
	}

	@Override
	public int size() {
		return pila.size();
	}

	@Override
	public boolean contains(Card<S, F> card) {
		if (pila.contains(card)) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public void putTop(Card<S, F> card) {
		pila.addLast(card);
	}

	@Override
	public void putBottom(Card<S, F> card) {
		pila.addFirst(card);
	}

	@Override
	public Card<S, F> takeTop() throws NoSuchElementException {
		 return pila.removeLast();
		//return pila.pollFirst();
	}

	@Override
	public boolean takeTop(Card<S, F> card) {
		return pila.removeLastOccurrence(card);
	}

	@Override
	public Card<S, F> takeBottom() throws NoSuchElementException {
		return pila.removeFirst();
	}

	@Override
	public boolean takeBottom(Card<S, F> card) {
		return pila.removeFirstOccurrence(card);
	}

	@Override
	public void shuffle() {
		Collections.shuffle(pila);
	}

}
