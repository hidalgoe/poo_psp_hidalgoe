package m3u5.impl.cards;

import java.util.Iterator;

import m3u5.prob.cards.Card;
import m3u5.prob.cards.CardPile;
import m3u5.prob.cards.CardsKind;

public class CardsKindImpl <S,F> implements CardsKind<S, F> {

	S[] ss;
	F[] fs;
		
	public CardsKindImpl(S[] coll, F[] figures) {
		super();
		this.ss = coll;
		this.fs = figures;	
	}

	@Override
	public S[] getSuits() {
		return ss;
	}

	@Override
	public F[] getFigures() {
		return fs;
	}

	@Override
	public Card<S, F> createCard(S suit, F figure) {
		
		Card<S, F> carta = new CardImpl<>(suit, figure);
		
		return carta;
	}

	@Override
	public CardPile<S, F> createFilledPile() {
		CardPile<S, F> pila = new CardPileImpl<S, F>();
		for (F f: fs  ) {
			for(S s: ss) {
				
				Card<S, F>carta = new CardImpl<>(s, f); 
				pila.putBottom(carta);
				
			}
			
		}
		
		return pila;
	}

	@Override
	public CardPile<S, F> createEmptyPile() {
		CardPile<S, F> pila = new CardPileImpl<S, F>();		
		return pila;
	}


}
