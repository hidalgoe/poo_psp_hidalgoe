package m3u5.impl.figures;

import java.util.Iterator;
import java.util.Random;

import m3u5.prob.figures.Turtle;
import m3u5.prob.figures.TurtleFigure;
import m3u5.prob.figures.TurtleFigureSet;

public class TurtleFigureSetImpl implements TurtleFigureSet {

	public static class Spiral implements TurtleFigure {

		@Override
		public void draw(Turtle t) {

			t.reset();
			t.up();
			t.forward(1);
			t.left(90);
			t.forward(1);
			t.right(90);

			t.down();
			t.forward(98);
			t.left(90);
			t.forward(98);

			int n = 98;

			while (n != 0) {

				t.left(90);
				t.forward(n);

				n--;
			}
		}
	}

	public static class Tree implements TurtleFigure {

		@Override
		public void draw(Turtle t) {

			int longi = 30, ang = 20;

			int prof = 0;
			t.reset();

			t.up();
			t.forward(50);
			t.left(90);
			t.down();
			t.forward(longi);

			DibujaArbol(t, prof, ang, longi);

		}

		public void DibujaArbol(Turtle t, int prof, int ang, int longi) {
			prof++;
			if (prof < 6) {

				longi = (int) (longi / 1.4);
				GiraD(t, ang, longi);
				DibujaArbol(t, prof, ang, longi);
				t.backward(longi);
				GiraI(t, ang * 2, longi);
				DibujaArbol(t, prof, ang, longi);
				t.backward(longi);
				t.right(ang);
			}

		}

		public void GiraD(Turtle t, int ang, int longi) {

			t.right(ang);
			t.forward(longi);

		}

		public void GiraI(Turtle t, int ang, int longi) {

			t.left(ang);
			t.forward(longi);
		}

	}

	public static class FreeStyle implements TurtleFigure {

		@Override
		public void draw(Turtle t) {

			Random rnd = new Random();
			int x = rnd.nextInt(100);

			for (int i = 0; i < 50; i++) {

				int longi = rnd.nextInt(10);
				int altura = rnd.nextInt(10);

				t.reset();
				t.up();
				t.forward(longi * 10);
				t.left(90);
				t.forward(altura * 10);
				t.right(90);

				for (int z = 0; z < 9; z++) {
					stars(t, x / 4);
				}
			}

		}

		public void stars(Turtle t, int tama�o) {

			t.down();

			triangulo(t, tama�o);
			t.up();
			t.forward(tama�o / 2);
			t.right(90);
			t.forward(tama�o / 4);
			t.right(210);
			t.down();
			triangulo(t, tama�o);

		}

		public void triangulo(Turtle t, int tama�o) {

			for (int i = 0; i < 3; i++) {
				t.forward(tama�o);
				t.left(120);
			}

		}

	}

	@Override
	public TurtleFigure createSpiralFigure() {

		TurtleFigure spiral = new Spiral();
		return spiral;
	}

	@Override
	public TurtleFigure createTreeFigure() {

		TurtleFigure tree = new Tree();
		return tree;
	}

	@Override
	public TurtleFigure createFreeStyleFigure() {

		TurtleFigure fs = new FreeStyle();
		return fs;
	}

}
