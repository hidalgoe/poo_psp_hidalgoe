package m3u5.impl.figures;

import m3u5.prob.canvas.DrawCanvas;
import m3u5.prob.figures.Turtle;

public class TurtleImpl implements Turtle {

	DrawCanvas canvas;

	boolean draw;
	private double x;
	private double y;
	private double angulo;

	public TurtleImpl(DrawCanvas canvas) {

		this.canvas = canvas;
	}

	@Override
	public void up() {

		draw = false;

	}

	@Override
	public void down() {

		draw = true;

	}

	@Override
	public double x() {
		return x;
	}

	@Override
	public double y() {
		return y;
	}

	@Override
	public double angle() {
		return angulo;
	}

	@Override
	public void left(double delta) {
		angulo = angulo + delta;

	}

	@Override
	public void right(double delta) {
		angulo = angulo - delta;

	}

	@Override
	public void forward(double step) {
		double e = x;
		double f = y;;

		x = x + step * Math.cos(Math.toRadians(angulo));
		y = y + step * Math.sin(Math.toRadians(angulo));

		if (draw) {
			canvas.drawLine(e, f, x, y);
		}

	}

	@Override
	public void backward(double step) {
		forward(-step);

	}

	@Override
	public void reset() {
		x=0;
		y=0;
		angulo=0;

	}

	@Override
	public DrawCanvas getDrawCanvas() {
		return canvas;
	}

}
