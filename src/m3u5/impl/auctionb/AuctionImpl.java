package m3u5.impl.auctionb;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import m3u5.prob.auctionb.Auction;
import m3u5.prob.auctionb.Bid;
import m3u5.prob.auctionb.Bidder;

public class AuctionImpl implements Auction {

	private double sp;
	private Bid top;

	Map<Bidder, Bid> apuestas;
	Map<Bidder, Boolean> banned;

	public AuctionImpl(double startingPrice) {
		this.sp = startingPrice;
		apuestas = new HashMap<Bidder, Bid>();
		banned = new HashMap<Bidder, Boolean>();
		top = new Bid(null, startingPrice);
	}

	@Override
	public double getStartingPrice() {
		return sp;
	}

	@Override
	public Bid getTopBid() {
		if (apuestas.size() > 0) {
			for (Entry<Bidder, Bid> ap : apuestas.entrySet()) {
				if (ap.getValue().getOffer() > top.getOffer()) {
					top = ap.getValue();
					return top;
				}
			}
		} else {
			top = null;
		}
		return top;

	}

	@Override
	public Bid getTopBid(Bidder b) {
		if (banned.containsKey(b)) {
			throw new IllegalStateException("");
		}
		return apuestas.get(b);
	}

	@Override
	public boolean isAcceptable(Bid b) {

		if (apuestas.size() > 0) {
			for (Entry<Bidder, Bid> ap : apuestas.entrySet()) {
				if (ap.getValue().getOffer() > top.getOffer()) {
					top = ap.getValue();
				}
			}
		}

		if (apuestas.size() == 0 && b.getOffer() >= sp) {
			top = b;
			apuestas.put(b.getBidder(), b);
			return true;
		} else if (apuestas.size() > 0 && b.getOffer() > top.getOffer()) {
			apuestas.put(b.getBidder(), b);
			return true;
		} else {
			return false;
		}

	}

	@Override
	public boolean bid(Bid b) {

		if (isBanned(b.getBidder())) {
			throw new IllegalStateException("");
		}
		if (isAcceptable(b) && banned.get(b) == null) {
			apuestas.remove(b.getBidder());
			apuestas.put(b.getBidder(), b);
			return true;
		} else {
			return false;

		}

	}

	@Override
	public boolean ban(Bidder b) {

		if (isBanned(b)) {
			return false;
		}

		banned.put(b, true);

		if (apuestas.containsKey(b)) {
			apuestas.remove(b);
			return true;
		}

		return true;
	}

	@Override
	public boolean isBanned(Bidder b) {
		if (banned.containsKey(b)) {
			return true;
		} else {
			return false;

		}
	}

}
