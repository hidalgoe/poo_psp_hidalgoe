package m3u5.impl.shoplist;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import m3u5.prob.shoplist.ShopList;
import m3u5.prob.shoplist.ShopProduct;

public class ShopListImpl implements ShopList {

	Map<String, ShopProduct> map;

	ShopProduct shp;

	public ShopListImpl() {

		map = new HashMap<String, ShopProduct>();

	}

	@Override
	public Iterator<ShopProduct> iterator() {

		return map.values().iterator();
	}

	@Override
	public int size() {
		return map.size();
	}

	@Override
	public ShopProduct findProduct(String name) {

		if (map.containsKey(name)) {
			return map.get(name);
		} else {
			return null;
		}

	}

	@Override
	public boolean addProduct(ShopProduct p) {

		String nom = p.getName();

		if (map.containsKey(nom) && findProduct(nom).equals(p)) {
			return false;
		} else {
			map.put(nom, p);
			return true;
		}

	}

	@Override
	public boolean removeProduct(String name) {

		if (map.containsKey(name)) {
			map.remove(name);
			return true;
		} else {
			return false;
		}

	}

	@Override
	public int addUnits(String name, int units) {

		double x = 0;
		int y = 0;

		if (map.containsKey(name)) {

			x = map.get(name).getUnitPrice();
			y = map.get(name).getUnits() + units;
			shp = new ShopProductImpl(name, x, y);

			map.put(name, shp);
			return map.get(name).getUnits();

		} else {
			return -1;
		}
	}

	@Override
	public int removeUnits(String name, int units) {

		double x = 0;
		int y = 0;

		if (map.containsKey(name)) {

			x = map.get(name).getUnitPrice();
			y = map.get(name).getUnits() - units;
			shp = new ShopProductImpl(name, x, y);

			if (map.get(name).getUnits() >= units) {
				map.put(name, shp);
				return map.get(name).getUnits();
			} else {
				return -1;
			}

		} else {
			return -1;
		}
	}

	@Override
	public boolean setUnitPrice(String name, double price) {

		int y = 0;

		if (map.containsKey(name)) {

			y = map.get(name).getUnits();
			shp = new ShopProductImpl(name, price, y);
			map.put(name, shp);
			return true;

		}

		return false;
	}

}
