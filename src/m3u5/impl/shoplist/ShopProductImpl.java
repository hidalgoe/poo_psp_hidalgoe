package m3u5.impl.shoplist;

import java.util.Objects;

import m3u5.prob.shoplist.ShopProduct;

public class ShopProductImpl implements ShopProduct {

	private String name;
	private double price;
	private int units;
	
	
	public ShopProductImpl(String name, double price, int units) {
		super();
		this.name = name;
		this.price = price;
		this.units = units;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public double getUnitPrice() {
		return price;
	}

	@Override
	public int getUnits() {
		return units;
	}

	@Override
	public int hashCode() {
		return Objects.hash(name);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ShopProductImpl other = (ShopProductImpl) obj;
		return Objects.equals(name, other.name);
	}

	@Override
	public String toString() {
		return "ShopProductImpl [name=" + name + ", price=" + price + ", units=" + units + "]";
	}

	
}
