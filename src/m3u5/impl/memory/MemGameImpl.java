package m3u5.impl.memory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import m3u5.prob.memory.MemCard;
import m3u5.prob.memory.MemGame;
import m3u5.prob.memory.MemGameState;

public class MemGameImpl implements MemGame {

	private MemGameState state;

	private Map<Integer, String> map;
	private Map<Integer, Integer> points;

	private List<MemCard> cards;

	private int playerId = 0;
	private int p = 0;
	private int totalcards;
	private int turn = 0;

	private int pos1 = 0;
	private int pos2 = 0;

	private String card1 = null;
	private String card2 = null;

	public MemGameImpl() {
		state = state.INIT;
		map = new HashMap<Integer, String>();
		points = new HashMap<Integer, Integer>();
		cards = new ArrayList<MemCard>();

	}

	@Override
	public int join(String name) {

		int n = playerId;
		map.put(n, name);
		points.put(n, p);
		playerId++;
		return n;
	}

	@Override
	public void start(List<String> values) {

		totalcards = values.size();

		for (int i = 0; i < values.size(); i++) {
			MemCard mc = new MemCard(values.get(i), false);
			cards.add(mc);
		}

		state = MemGameState.LEFT_2;
	}

	@Override
	public void play(int player, int pos) {

		if (state == MemGameState.LEFT_2) {

			cards.set(pos, new MemCard(cards.get(pos).value(), true));
			card1 = cards.get(pos).value();
			state = MemGameState.LEFT_1;
			pos1 = pos;
		} else if (state == MemGameState.LEFT_1) {

			cards.set(pos, new MemCard(cards.get(pos).value(), true));
			card2 = cards.get(pos).value();
			pos2 = pos;

			if (card1.equals(card2)) {

				state = MemGameState.LEFT_2;
				points.remove(player);
				points.put(player, p++);
				boolean quedan = false;

				for (MemCard mc : cards) {
					if (mc.revealed() == false) {
						quedan = true;
					}
				}

				if (quedan) {
					state = MemGameState.LEFT_2;
					points.put(player, p++);
				} else {
					state = MemGameState.FINISHED;
				}

			} else {
				state = MemGameState.LEFT_0;
				Timer timer = new Timer();
				timer.schedule(new TimerTask() {
					@Override
					public void run() {
						cards.set(pos1, new MemCard(card1, false));
						cards.set(pos2, new MemCard(card2, false));
						state = MemGameState.LEFT_2;
						if(turn == 1) {
							turn = 0;
						}else if(turn == 0) {
							turn++;
						}
						
					}
				}, 1500);

			}
		}

	}

	@Override
	public MemCard getCard(int idx) {

		return cards.get(idx);

	}

	@Override
	public String getPlayer(int idx) {
		return map.get(idx);
	}

	@Override
	public int getScore(int player) {
		return points.get(player);
	}

	@Override
	public MemGameState getState() {
		return state;
	}

	@Override
	public int getTurn() {
		return turn;
	}

	@Override
	public int getPlayersCount() {
		return playerId;
	}

	@Override
	public int getCardsCount() {
		return totalcards;
	}

}
