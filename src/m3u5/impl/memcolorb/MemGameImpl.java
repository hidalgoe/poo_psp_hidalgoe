package m3u5.impl.memcolorb;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Logger;

import m3u5.prob.memcolorb.MemGame;
import m3u5.prob.memcolorb.MemGameListener;
import m3u5.prob.memory.MemCard;
import m3u5.prob.memory.MemGameState;

public class MemGameImpl implements MemGame {

	 static final Logger LOGGER = Logger.getLogger(MemGameImpl.class.getName());
	    
	    static final long UNREVEAL_MILLIS = 1500;
	    static final long TIMEOUT_MILLIS = 3000;
	    static final boolean RANDOM_START = false; // set to true for random first turn
	    
	    private int turn, prevturn, left, pos1, pos2;
	    private List<MemCard> cards;
	    private List<String> names;
	    private List<Integer> scores;
	    private MemGameState state;
	    private Timer timer;
	    private List<MemGameListener> listeners;
	    private Random random;
	    
	    public MemGameImpl() {        
	        
	        names = new ArrayList<>();
	        scores = new ArrayList<>();
	        listeners = new ArrayList<>();
	        this.state = MemGameState.INIT; // not notified
	        random = new Random();
	    }
	    
	    @Override
	    public void addListener(MemGameListener listener) {
	        this.listeners.add(listener);
	    }
	    
	    @Override
	    public void stop() {
	        cancelTimeout();
	    }

	    @Override
	    public int join(String name) {
	        
	        names.add(name);
	        scores.add(0);
	        int player = names.size() - 1;
	        listeners.forEach(listener -> listener.onJoin(player, name));
	        return player;
	    }

	    @Override
	    public void start(List<String> values) {
	        
	        if (names.size() < 2) {
	            throw new IllegalStateException("missing players: " + names);
	        }
	        
	        this.cards = new ArrayList<>();
	        for (int i=0; i<values.size(); i++) {
	            cards.add(new MemCard(values.get(i), false));
	        }
	        for (int i=0; i<scores.size(); i++) {
	        	scores.set(i, 0);
	        	int player = i;
	        	listeners.forEach(listener -> listener.onScore(player, 0));
	        }
	        this.left = cards.size();
	        
	        if (this.state == MemGameState.FINISHED) { // restart         
	            int oldturn = this.turn;
	            this.turn = (this.prevturn + 1) % names.size();
	            if (oldturn != this.turn) {
	                listeners.forEach(listener -> listener.onTurn(oldturn, false));
	            }
	        }
	        else {
	            this.turn = RANDOM_START? random.nextInt(names.size()) : 0;
	        }
	        this.prevturn = turn;
	        
	        this.state = MemGameState.LEFT_2;
	        scheduleTimeOut();
	        listeners.forEach(listener -> {            
	            listener.onState(state);
	            listener.onTurn(turn, true);
	        });
	    }
	    
	    @Override
	    public void play(int player, int pos) {

	        if (state == MemGameState.FINISHED || state == MemGameState.LEFT_0) {
	            throw new IllegalStateException(player + " cannot play while in state " + state);
	        }
	        
	        if (player != turn) {
	            throw new IllegalStateException("not your turn (" + player + "), it's " + turn);
	        }
	        
	        MemCard card = cards.get(pos);
	        if (card.revealed()) {
	            throw new IllegalStateException("card already revealed!");
	        }
	        
	        LOGGER.info(player + " playing " + pos + " in state " + state);

	        cancelTimeout();
	        
	        left -= 1;
	        cards.set(pos, new MemCard(card.value(), true));
	        listeners.forEach(listener -> listener.onReveal(player, pos, card.value()));
	        ////////////////////////////////////////////////////////////////////////////
	        
	        if (state == MemGameState.LEFT_1) {
	            pos2 = pos;
	            MemCard first = cards.get(pos1);
	            if (first.value().equals(card.value())) {
	                int score = scores.get(turn) + 1;
	                scores.set(turn, score);
	                listeners.forEach(listener -> listener.onScore(player, score));
	                if (left == 0) {
	                    state = MemGameState.FINISHED;
	                    listeners.forEach(listener -> listener.onState(state));
	                }
	                else {
	                    state = MemGameState.LEFT_2;
	                    scheduleTimeOut();
	                    listeners.forEach(listener -> listener.onState(state));
	                }
	            }
	            else {
	                state = MemGameState.LEFT_0;
	                scheduleUnreveal();
	                int oldturn = turn;
	                turn = (turn + 1) % names.size();
	                listeners.forEach(listener -> {
	                    listener.onState(state);
	                    listener.onTurn(oldturn, false);
	                    listener.onTurn(turn, true);    
	                });                
	            }
	        }
	        else if (state == MemGameState.LEFT_2) {
	            pos1 = pos;
	            state = MemGameState.LEFT_1;
	            scheduleTimeOut();
	            listeners.forEach(listener -> listener.onState(state));
	        }
	        else {
	            throw new IllegalStateException("impossible state!");
	        }
	    }
	    
	    private void scheduleTimeOut() {
	        
	        timer = new Timer();        
	        timer.schedule(new TimerTask() {
	            @Override
	            public void run() {
	                timer.cancel();
	                LOGGER.info("timer timed out for " + turn + "!");
	                if (state != MemGameState.LEFT_2) {
	                    unreveal(pos1);
	                    state = MemGameState.LEFT_2;
	                    listeners.forEach(listener -> listener.onState(state));
	                }
	                scheduleTimeOut();
	                int oldturn = turn;
	                turn = (turn + 1) % names.size();
	                listeners.forEach(listener -> {
	                    listener.onTurn(oldturn, false);
	                    listener.onTurn(turn, true);
	                });
	            }
	        }, TIMEOUT_MILLIS);        
	    }
	    
	    private void cancelTimeout() {
	        
	        if (timer != null) {
	            LOGGER.info("cancelling!");
	            timer.cancel();
	        }
	        else {
	            LOGGER.info("cannot cancel!");
	        }
	    }

	    private void scheduleUnreveal() {
	        
	        LOGGER.info("scheduling unreveal for " + pos1 + ", " + pos2);
	        
	        timer = new Timer();        
	        timer.schedule(new TimerTask() {
	            @Override
	            public void run() {
	                timer.cancel();
	                unreveal(pos1);
	                unreveal(pos2);
	                state = MemGameState.LEFT_2;
	                scheduleTimeOut();
	                listeners.forEach(listener -> listener.onState(state));
	            }
	        }, UNREVEAL_MILLIS);
	    }
	    
	    private void unreveal(int pos) {
	        left += 1;
	        MemCard card = cards.get(pos);
	        cards.set(pos, new MemCard(card.value(), false));
	        listeners.forEach(listener -> listener.onUnreveal(pos));        
	    }
	    
	    // QUERIES
	    
	    @Override
	    public MemCard getCard(int idx) {
	        return cards.get(idx);        
	    }
	    
	    @Override
	    public String getPlayer(int idx) {
	        return names.get(idx);
	    }
	    
	    @Override
	    public int getScore(int player) {
	        return scores.get(player);
	    }

	    @Override
	    public MemGameState getState() {
	        return state;
	    }

	    @Override
	    public int getTurn() {
	        return turn;
	    }

	    @Override
	    public int getPlayersCount() {
	        return names.size();
	    }

	    @Override
	    public int getCardsCount() {
	        return cards.size();
	    }

	    @Override
	    public String toString() {
	        return cards != null? cards.toString() : "";
	    }

}
