package m3u5.impl.memcolorb;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import m3u5.prob.memcolorb.MemUI;
import m3u5.prob.memcolorb.MemUI.MemViewListener;

public class MemViewImpl implements MemUI.MemView {

	static final Logger LOGGER = Logger.getLogger(MemViewImpl.class.getName());

	static final int WIDTH = 800;
	static final int HEIGHT = 400;

	private MemViewListener listener;

	private int joinCount;
	private TextField nameTxt;
	private Label statusLbl, namesLbl, msgLbl, yesornot;
	private Button overBtn, startBtn;
	private Button[] buttons;
	private List<Label> scores;
	private Stage stage;

	public MemViewImpl(Stage stage) {

		this.scores = new ArrayList<>();
		this.stage = stage;
		stage.setTitle("Memory game");
	}

	@Override
	public void setListener(MemViewListener listener) {
		this.listener = listener;
	}

	@Override
	public void showStartView() {
		stage.setScene(createNamesScene());
		stage.show();
	}

	@Override
	public void showBoardView(int count, List<String> names) {
		stage.setScene(createCardsScene(count, names));
	}

	@Override
	public void showJoin(String name) {

		joinCount++;
		msgLbl.setText(name + " joined!");
		nameTxt.setText("");
		if (joinCount == 1) {
			namesLbl.setText(name);
		} else {
			startBtn.setDisable(false);
			namesLbl.setText(namesLbl.getText() + ", " + name);
		}
	}

	@Override
	public void setStatus(String text) {
		statusLbl.setText(text);
	}

	@Override
	public void setTurn(int player) {
		boldScore(player, true);
	}

	@Override
	public void unsetTurn(int player) {
		boldScore(player, false);
		yesornot.setVisible(true);
		yesornot.setText("No!!");
	}

	private void boldScore(int player, boolean bold) {
		scores.get(player).setStyle(bold ? "-fx-font-weight: bold;" : "-fx-font-weight: normal;");

	}

	@Override
	public void setScore(int player, int score) {

		if (scores.get(player).getText() == "-") {

		} else if (Integer.parseInt(scores.get(player).getText()) < score) {
			yesornot.setVisible(true);
			yesornot.setText("Yes!!");
		} else if (Integer.parseInt(scores.get(player).getText()) >= score) {
			yesornot.setVisible(true);
			yesornot.setText("No!!");
		}
		scores.get(player).setText(Integer.toString(score));

	}

	@Override
	public void showCard(int player, int position, String value) {

		yesornot.setVisible(false);
		if (player % 3 == 0) {
			buttons[position].setStyle("-fx-background-color: green;");
			buttons[position].setText(value);
			buttons[position].setDisable(true);
			buttons[position].setOpacity(1);

		} else if (player % 3 == 1) {
			buttons[position].setStyle("-fx-background-color: red;");
			buttons[position].setText(value);
			buttons[position].setDisable(true);
			buttons[position].setOpacity(1);
		} else if (player % 3 == 2) {
			buttons[position].setStyle("-fx-background-color: blue;");
			buttons[position].setText(value);
			buttons[position].setDisable(true);
			buttons[position].setOpacity(1);
		}

	}

	@Override
	public void hideCard(int position) {
		Platform.runLater(() -> {
			buttons[position].setDisable(false);
			buttons[position].setText("?");
			buttons[position].setOpacity(0.5);
			buttons[position].setStyle("");
		});
	}

	@Override
	public void askBoardRestart(boolean enable) {
		overBtn.setVisible(enable);
	}

	@Override
	public void resetBoard() {

		statusLbl.setText("");
		for (Label lbl : scores) {
			lbl.setText("-");
		}
		for (Button btn : buttons) {
			btn.setText("?");
			btn.setDisable(false);
			btn.setOpacity(0.5);
			btn.setStyle("");
			yesornot.setVisible(false);
		}
	}

	// SCENES

	private Scene createNamesScene() {

		VBox root = new VBox(10);
		root.setAlignment(Pos.CENTER);
		root.setStyle("-fx-font-size: 24pt");

		msgLbl = new Label("Enter player names");

		HBox namesBox = new HBox(20);
		namesBox.setAlignment(Pos.CENTER);
		namesLbl = new Label("-");
		namesBox.getChildren().addAll(new Label("Joined:"), namesLbl);

		HBox inputBox = new HBox(20);
		inputBox.setAlignment(Pos.CENTER);
		nameTxt = new TextField();
		Button joinBtn = new Button("Join");
		joinBtn.setDefaultButton(true);
		joinBtn.setOnAction(event -> {
			if (!isValidName(nameTxt.getText())) {
				msgLbl.setText("Invalid name!");
			} else {
				listener.onJoinAction(nameTxt.getText(), true);
			}
		});
		inputBox.getChildren().addAll(new Label("Name:"), nameTxt, joinBtn);

		startBtn = new Button("Start");
		startBtn.setDisable(true);
		startBtn.setOnAction(event -> listener.onStartAction());

		root.getChildren().addAll(msgLbl, namesBox, inputBox, startBtn);
		return new Scene(root, WIDTH, HEIGHT);
	}

	private boolean isValidName(String name) {
		return name.length() > 0;
	}

	private Scene createCardsScene(int count, List<String> names) {

		VBox root = new VBox(10);
		root.setAlignment(Pos.CENTER);
		root.setStyle("-fx-font-size: 24pt");

		HBox scoresPane = new HBox(20);
		scoresPane.setAlignment(Pos.CENTER);

		for (String name : names) {
			Label scorelbl = new Label("-");
			scores.add(scorelbl);
			scoresPane.getChildren().addAll(new Label(name), scorelbl);
		}

		FlowPane cardsPane = new FlowPane(2, 2);
		cardsPane.setAlignment(Pos.CENTER);

		buttons = new Button[count];
		cardsPane.getChildren().clear();
		for (int i = 0; i < count; i++) {
			Button btn = new Button("?");
			btn.setOpacity(0.5);
			int i2 = i;
			btn.setOnAction(event -> listener.onCardAction(i2));
			buttons[i] = btn;
			cardsPane.getChildren().add(buttons[i]);
		}

		yesornot = new Label();

		statusLbl = new Label();
		overBtn = new Button("Restart");
		overBtn.setVisible(false);
		overBtn.setOnAction(event -> listener.onRestartAction());

		root.getChildren().addAll(scoresPane, statusLbl, yesornot, cardsPane, overBtn);
		return new Scene(root, WIDTH, HEIGHT);
	}

}
