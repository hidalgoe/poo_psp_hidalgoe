package m3u5.impl.canvas;

import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import m3u5.prob.canvas.DrawCanvas;
import m3u5.prob.canvas.DrawColor;

public class DrawCanvasAwt implements DrawCanvas {

	private double scale;
	private double size;

	int x;
	int y;
	
	Graphics2D g;
	double width;
	Stroke stroke;
	BufferedImage image;

	public DrawCanvasAwt(int size, int scale) {

		this.size = size;
		this.scale = scale;
		
		x = size/scale;
		y = (scale-y) * size/scale;
		
		image = new BufferedImage(size, size, BufferedImage.TYPE_INT_ARGB);
		g = image.createGraphics();
		g.setPaint(Color.BLACK);
		

	}

	@Override
	public double getScale() {
		return scale;
	}

	@Override
	public void setColor(DrawColor color) {

		Color colorin = new Color(color.getRed(),color.getGreen(), color.getBlue());
		
		g.setColor(colorin);

	}

	@Override
	public void setPenSize(double size) {

		width = size * (this.size/this.scale);
		
		
	}

	@Override
	public void drawPoint(double x, double y) {

		x = x * (size/scale) - (x/2);
		y = (scale - y) * (size/scale) - (y/2);
		
		Ellipse2D.Double circle = new Ellipse2D.Double(x, y, width, width);
		g.fill(circle);

	}

	@Override
	public void drawLine(double x0, double y0, double x1, double y1) {

		
		x0 = (x0 * (size/scale));
		y0 = (scale - y0) * (size/scale);
		
		x1 = (x1 * (size/scale));
		y1 = (scale - y1) * (size/scale);
		
		Stroke stroke = new BasicStroke((int) width, BasicStroke.CAP_ROUND, BasicStroke.JOIN_MITER);
		g.setStroke(stroke);
		g.drawLine( (int) x0, (int) y0, (int) x1, (int) y1);


	}

	@Override
	public void save(String filename) {

		File imageFile = new File(System.getProperty("user.dir") + File.separator + "data" + File.separator + "draw"
				+ File.separator + filename);

		try {
			ImageIO.write(image, "png", imageFile);
		} catch (IOException e) {
			throw new RuntimeException("problem saving " + imageFile, e);
		}

	}

	@Override
	public void clear() {
		g.setPaint(Color.WHITE);
		g.fillRect(0, 0, image.getWidth(), image.getHeight());
		g.setColor(Color.BLACK);
	}

	@Override
	public void close() {
		
	}

}
