package m3u5.impl.tickets;

import java.util.UUID;

import m3u5.prob.tickets.Ticket;
import m3u5.prob.tickets.TicketOffice;

public class TicketOfficeImpl implements TicketOffice {

	private int size;
	private int rest;

	private Ticket[] llista;

	public TicketOfficeImpl(int size) {
		this.size = size;
		this.rest = size;
		llista = new Ticket[size];
	}

	@Override
	public Ticket buy() {

		UUID uuid = UUID.randomUUID();

		for (int i = 0; i < size; i++) {
			
			if (llista[i] == null) {
				llista[i] = new Ticket(i, uuid);
				rest--;
				return new Ticket(i, uuid);
			}
		}

		return null;
	}

	@Override
	public Ticket buy(int seat) {
		UUID uuid = UUID.randomUUID();

		if (llista[seat] == null) {
			llista[seat] = new Ticket(seat, uuid);
			rest--;
			return new Ticket(seat, uuid);
		} else {
			return null;
		}
	}

	@Override
	public boolean isFree(int seat) {

		if (llista[seat] == null) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public boolean validate(Ticket ticket) {

		for (int i = 0; i < size; i++) {

			if (llista[i] != null && llista[i].getCode().equals(ticket.getCode())) {

				return true;
			}
		}

		return false;

	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public int left() {
		return rest;
	}

}
