package m9u1.impl.msgpeer;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Logger;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

import lib.Asserts;
import lib.CryptoKeys;
import m9u1.prob.msgpeer.MsgHandler;
import m9u1.prob.msgpeer.MsgPeer;
import m9u1.prob.msgpeer.impl.MsgHelper;
import m9u1.prob.msgpeer.impl.MsgUnsafePeerImpl;
import m9u3.base.BaseUDPPeer;

public class MsgSafePeerImpl extends BaseUDPPeer implements MsgPeer {

	static final Logger LOGGER = Logger.getLogger(MsgUnsafePeerImpl.class.getName());
	static final Charset CHARSET = StandardCharsets.UTF_8;
	static final int TIMEOUT = 1500;

	static final byte CONNECT_CMD = 1;
	static final byte CONNACK_CMD = 2;
	static final byte MESSAGE_CMD = 3;
	static final byte DISCONNECT_CMD = 4;

	static final String symetric = "AES/CBC/PKCS5Padding";
	static final String asymetric = "RSA/ECB/PKCS1Padding";

	private MsgHandler handler;

	KeyPairGenerator kpg;
	Key pub, pri;
	KeyPair kp;

	private Map<String, Key> peers; // value is true if ACK received

	public MsgSafePeerImpl(String host, int port, MsgHandler handler) {

		super(host, port, TIMEOUT);
		this.handler = handler;
		peers = new ConcurrentHashMap<>();

		try {
			kpg = KeyPairGenerator.getInstance("RSA");
			kpg.initialize(2048);

			kp = kpg.generateKeyPair();

			pub = kp.getPublic();
			pri = kp.getPrivate();

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void connect(String toHost) {

		Asserts.isFalse(peers.containsKey(toHost), "already exists " + toHost);

		try {

			send(toHost, CONNECT_CMD, pub.getEncoded());

		} catch (IOException e) {
			e.printStackTrace();
		}
		// peers.put(toHost, pub); // ACK pending
		//LOGGER.info(this.host + " added peer " + toHost + ": " + peers);
	}

	@Override
	public void disconnect(String toHost) {

		// Asserts.isTrue(peers.get(toHost), "unconnected " + toHost);

		try {
			send(toHost, DISCONNECT_CMD, new byte[] {});
			peers.remove(toHost);
			LOGGER.info(this.host + " removed peer " + toHost + ": " + peers);

		} catch (IOException e) {
			throw new RuntimeException("linking to " + toHost, e);
		}
	}

	@Override
	public void message(String toHost, String message) {

		try {
			byte[] bytesXifrat = null;
			
			Cipher cipher = Cipher.getInstance(symetric);

			cipher.init(Cipher.ENCRYPT_MODE, peers.get(toHost));
			bytesXifrat = cipher.doFinal(message.getBytes(CHARSET));
			LOGGER.info("==> xifrat: " + CryptoKeys.pretty(bytesXifrat));

			send(toHost, MESSAGE_CMD, bytesXifrat);

		} catch (IOException | GeneralSecurityException e) {
			throw new RuntimeException("reading message", e);
		}
	}

	@Override
	protected void handle(DatagramPacket packet) {

		byte[] input = MsgHelper.fromPacket(packet);
		InetAddress address = packet.getAddress();
		LOGGER.info(this.host + " received [" + input.length + "] from " + address + ":"
				+ CryptoKeys.pretty(input, 16, 64));

		byte command = input[0];
		byte[] body = MsgHelper.fromPosition(input, 1);
		String fromHost = address.getHostAddress();
		boolean exists = peers.containsKey(fromHost);

		switch (command) {
		case CONNECT_CMD:

			SecretKey keyGen;
			Asserts.isFalse(exists);
			try {

				keyGen = CryptoKeys.generateSecretKey("AES", 256);
				KeyFactory kf = KeyFactory.getInstance("RSA");
				PublicKey toCipher = CryptoKeys.importPublicKey(kf, body);
				Cipher cipher = Cipher.getInstance(asymetric);

				cipher.init(Cipher.ENCRYPT_MODE, toCipher);

				byte[] bytesXifrat = cipher.doFinal(keyGen.getEncoded());

				send(fromHost, CONNACK_CMD, bytesXifrat);

			} catch (IOException | NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException
					| IllegalBlockSizeException | BadPaddingException | InvalidKeySpecException e) {
				throw new RuntimeException("sending ack", e);
			}
			peers.put(fromHost, keyGen);
			LOGGER.info(this.host + " add peer " + fromHost + ": " + CryptoKeys.pretty(keyGen.getEncoded()));
			break;

		case CONNACK_CMD:

			SecretKey fin = null;

			byte[] bytesXifrat;
			try {

				PrivateKey toCipher = kp.getPrivate();

				Cipher cipher = Cipher.getInstance(asymetric);
				cipher.init(Cipher.DECRYPT_MODE, toCipher);

				bytesXifrat = cipher.doFinal(body);

				fin = CryptoKeys.importSecretKey("AES", bytesXifrat);

			} catch (GeneralSecurityException e) {
				e.printStackTrace();
			}

			peers.put(fromHost, fin);

			LOGGER.info(this.host + " ack peer " + fromHost + ": " + CryptoKeys.pretty(fin.getEncoded()));
			handler.onConnected(fromHost);
			break;
		case MESSAGE_CMD:
			Asserts.isTrue(exists);

			LOGGER.info("==> message: " + CryptoKeys.pretty(body));
			
			try {
				Cipher cyp = Cipher.getInstance(symetric);
				cyp.init(Cipher.DECRYPT_MODE, peers.get(fromHost));
				byte[] descifrat = cyp.doFinal(body);
				
				handler.onMessage(fromHost, new String(descifrat, CHARSET));
			} catch (GeneralSecurityException e) {
				e.printStackTrace();
			}

			break;
		case DISCONNECT_CMD:
			Asserts.isTrue(exists);
			peers.remove(fromHost);
			LOGGER.info(this.host + " disconnected peer " + fromHost + ": " + peers);
			break;
		default:
			Asserts.error("unexpected command " + command);
		}
	}

	private void send(String host, byte command, byte[] content) throws IOException {

		byte[] buf = MsgHelper.joinBytes(command, content);
		DatagramPacket dp = new DatagramPacket(buf, buf.length, InetAddress.getByName(host), port);
		socket.send(dp);
		LOGGER.info(this.host + " sending [" + buf.length + "] to " + host + ":" + CryptoKeys.pretty(buf, 16, 64));
	}
}
