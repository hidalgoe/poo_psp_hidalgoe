package m9u1.impl.msgpeer;

import m9u1.prob.msgpeer.MsgHandler;
import m9u1.prob.msgpeer.MsgPeer;
import m9u1.prob.msgpeer.MsgPeerFactory;

public class MsgPeerFactoryImpl implements MsgPeerFactory {

	@Override
	public MsgPeer createPeer(String host, int port, MsgHandler handler) {
		return new MsgSafePeerImpl(host, port, handler);
	}

}
