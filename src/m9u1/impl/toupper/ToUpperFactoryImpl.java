package m9u1.impl.toupper;

import lib.Startable;
import m9u1.prob.toupper.ToUpperFactory;
import m9u3.prob.toupper.ToUpperClient;

public class ToUpperFactoryImpl implements ToUpperFactory {

	@Override
	public ToUpperClient createClient(String host, int port, byte[] aesKey) {
		return new ToUpperClientImpl(host, port, aesKey);
	}

	@Override
	public Startable createServer(String host, int port, byte[] aesKey) {
		return new ToUpperServerImpl(host, port, aesKey);
	}

}
