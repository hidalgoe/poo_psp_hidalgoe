package m9u1.impl.toupper;

import java.io.IOException;
import java.net.Socket;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.logging.Logger;

import lib.CoDecException;
import m9u3.base.BaseTCPServer;
import m9u3.base.BaseTCPTextHandler;

public class ToUpperServerImpl extends BaseTCPServer {

	static final Logger LOGGER = Logger.getLogger(ToUpperServerImpl.class.getName());

	static final Charset CHARSET = StandardCharsets.UTF_8; // it could be UTF8, ISO8859_1...
	static final int TIMEOUT_MILLIS = 1500;
	byte[] aesKey;
	CoDeImpl ci;

	public ToUpperServerImpl(String host, int port, byte[] aesKey) {
		super(host, port, TIMEOUT_MILLIS);
		this.aesKey = aesKey;
		ci = new CoDeImpl(aesKey);

	}

	@Override
	protected Runnable createHandler(Socket clientSocket) {

		try {
			return new BaseTCPTextHandler(clientSocket) {
				@Override
				protected void handle(String input) throws IOException {

					
					String de = ci.decode(input);
					
					
					LOGGER.info("request is " + input);
					String output = de.toUpperCase();
					LOGGER.info("response is " + output);
					String en = ci.encode(output);
					out.println(en);
				}
			};
		} catch (IOException e) {
			throw new RuntimeException("creating handler", e);
		}
	}

}
