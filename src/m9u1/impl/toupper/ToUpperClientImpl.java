package m9u1.impl.toupper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.charset.Charset;
import java.util.logging.Logger;

import lib.Asserts;
import m9u3.prob.toupper.ToUpperClient;

public class ToUpperClientImpl implements ToUpperClient {

	static final Logger LOGGER = Logger.getLogger(ToUpperClientImpl.class.getName());

	static final Charset CHARSET = ToUpperServerImpl.CHARSET;
	static final int TIMEOUT_MILLIS = 1500;

	private Socket socket;
	private String host;
	private int port;
	private PrintWriter out;
	private BufferedReader in;
	byte[] aesKey;
	CoDeImpl ci;

	public ToUpperClientImpl(String host, int port, byte[] aesKey) {
		this.host = host;
		this.port = port;
		this.aesKey = aesKey;
		ci = new CoDeImpl(aesKey);
	}

	@Override
	public void connect() throws IOException {

		socket = new Socket();
		socket.connect(new InetSocketAddress(host, port), 1500); // connect timeout
		socket.setSoTimeout(TIMEOUT_MILLIS); // response timeout

		out = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), CHARSET), true);
		in = new BufferedReader(new InputStreamReader(socket.getInputStream(), CHARSET));
	}

	@Override
	public void disconnect() throws IOException {
		try {
			socket.close();
		} finally {
			socket = null;
		}
	}

	@Override
	public String toUpper(String input) throws IOException {

		Asserts.isNotNull(socket, "not connected");
		LOGGER.info("request is " + input);
		
		out.println(ci.encode(input));

		String output = in.readLine();
		
		if (output == null) {
			socket = null;
			throw new IOException("socket is not connected");
		}
		output = ci.decode(output);
		LOGGER.info("response is " + output);
		return output;
	}
}
