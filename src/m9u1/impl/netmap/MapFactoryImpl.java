package m9u1.impl.netmap;

import lib.Startable;
import m9u1.prob.netmap.MapClient;
import m9u1.prob.netmap.MapFactory;

public class MapFactoryImpl implements MapFactory {

	@Override
	public MapClient createClient(String host, int port, byte[] aesKey) {
		return new MapClientImpl(host, port, 1500, aesKey);
	}

	@Override
	public Startable createServer(String host, int port, byte[] aesKey) {
		return new MapServerImpl(host, port, 1500, aesKey);
	}

}
