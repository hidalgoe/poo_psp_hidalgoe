package m9u1.impl.netmap;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketTimeoutException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import lib.CoDecException;
import lib.Startable;

public class MapServerImpl implements Startable, Runnable {

	static final Logger LOGGER = Logger.getLogger(MapServerImpl.class.getName());
	static final Charset CHARSET = StandardCharsets.UTF_8;
	static final int RESPONSE_TIMEOUT = 1500;
	static final int SIZE = 1460;

	static final String GET_REGEXP = "^G([a-zA-Z0-9._-]{3,16})$";
	static final String SET_REGEXP = "^S([a-zA-Z0-9._-]{3,16}):(.+)$";

	private Map<String, String> map;

	private String host;
	private int port, timeout;
	private boolean stopping;
	private Thread thread;
	byte[] aesKey;

	private DatagramSocket socket;

	public MapServerImpl(String host, int port, int timeout, byte[] aesKey) {
		this.host = host;
		this.port = port;
		this.timeout = timeout;
		this.aesKey = aesKey;
		this.map = new HashMap<>();
	}

	@Override
	public void start() {

		try {
			socket = new DatagramSocket(port, InetAddress.getByName(host));
			socket.setSoTimeout(timeout);
		} catch (IOException e) {
			throw new RuntimeException("connecting socket", e);
		}

		thread = new Thread(this, "server");
		thread.start();
	}

	@Override
	public void stop() {

		stopping = true;
		if (socket != null) {
			socket.close();
		}
	}

	@Override
	public boolean isStarted() {
		return thread != null && thread.isAlive();
	}

	@Override
	public void run() {

		LOGGER.info("starting server at " + host + ":" + port);

		try {
			while (!stopping) {
				DatagramPacket packet = new DatagramPacket(new byte[SIZE], SIZE);
				try {
					socket.receive(packet);
					handle(packet);

				} catch (SocketTimeoutException e) {
					LOGGER.fine("timeout receiving!");

				} catch (RuntimeException | AssertionError e) {
					LOGGER.log(Level.SEVERE, "unexpected error", e);
				}
			}

		} catch (IOException e) {
			if (stopping) {
				LOGGER.info("stopping exception: " + e);
			} else {
				LOGGER.log(Level.SEVERE, "receiving", e);
			}

		} finally {
			LOGGER.info("done!");
		}
	}

	private void handle(DatagramPacket packet) {

		byte[] buf = new byte[packet.getLength()];
		System.arraycopy(packet.getData(), 0, buf, 0, packet.getLength());
		CoDeImpl ci = new CoDeImpl(aesKey);

		String command = null;
		try {
			command = ci.decode(buf);
		} catch (CoDecException e) {
			e.printStackTrace();
		}
		LOGGER.info("received " + Arrays.toString(buf) + ": " + command);
		if (buf.length == 0) {
			return;
		}

		Pattern pattern = Pattern.compile(GET_REGEXP);
		Matcher matcher = pattern.matcher(command);
		if (matcher.find()) {
			String key = matcher.group(1);
			LOGGER.info("getting " + key);
			String value = map.get(key);
			String result = value == null ? "!" : "=" + value;
			try {
				buf = ci.encode(result);
			} catch (CoDecException e) {
				e.printStackTrace();
			}
		} else {
			pattern = Pattern.compile(SET_REGEXP);
			matcher = pattern.matcher(command);
			if (matcher.find()) {
				String key = matcher.group(1);
				String value = matcher.group(2);
				LOGGER.info("setting " + key + "=" + value);
				map.put(key, value);
				buf = new byte[] { '=' };

				String b = new String(buf);
				try {
					buf = ci.encode(b);
				} catch (CoDecException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			} else {
				buf = new byte[] { '?' };
				String b = new String(buf);
				try {
					buf = ci.encode(b);
				} catch (CoDecException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		reply(packet, buf);
		LOGGER.info("sent " + Arrays.toString(buf));
	}

	private void reply(DatagramPacket packet, byte[] buf) {

		InetAddress address = packet.getAddress();
		int port = packet.getPort();

		packet = new DatagramPacket(buf, buf.length, address, port);
		try {
			socket.send(packet);
		} catch (IOException e) {
			throw new RuntimeException("sending " + Arrays.toString(buf), e);
		}
	}
}
