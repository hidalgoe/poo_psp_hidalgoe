package m9u1.impl.netmap;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

import lib.CoDec;
import lib.CoDecException;
import lib.CryptoKeys;

public class CoDeImpl implements CoDec<String, byte[]> {

	byte[] aesKey;
	Cipher cipher;

	public CoDeImpl(byte[] aesKey) {
		this.aesKey = aesKey;
		try {
			cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
		} catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public byte[] encode(String i) throws CoDecException {

		SecretKey sc = CryptoKeys.importSecretKey("AES", aesKey);
		byte[] bytesXifrat = null;

		try {
			cipher.init(Cipher.ENCRYPT_MODE, sc);
			bytesXifrat = cipher.doFinal(i.getBytes());
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			e.printStackTrace();
		} catch (BadPaddingException e) {
			e.printStackTrace();
		}

		return bytesXifrat;
	}

	@Override
	public String decode(byte[] o) throws CoDecException {

		SecretKey sc = CryptoKeys.importSecretKey("AES", aesKey);
		byte[] bytesXifrat = null;

		try {
			cipher.init(Cipher.DECRYPT_MODE, sc);
			bytesXifrat = cipher.doFinal(o);
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			e.printStackTrace();
		} catch (BadPaddingException e) {
			e.printStackTrace();
		}

		String f = new String(bytesXifrat);

		return f;
	}

}
