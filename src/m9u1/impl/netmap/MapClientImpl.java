package m9u1.impl.netmap;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.logging.Logger;

import m9u1.prob.netmap.MapClient;

public class MapClientImpl implements MapClient {

	static final Logger LOGGER = Logger.getLogger(MapClientImpl.class.getName());
	static final int RESPONSE_TIMEOUT = 1500;
	static final int SIZE = 1460;
	static final Charset CHARSET = StandardCharsets.UTF_8;

	private int timeout;

	private InetAddress address;
	private int port;
	private DatagramSocket socket;
	byte[] aesKey;

	public MapClientImpl(String host, int port, int timeout, byte[] aesKey) {

		try {
			this.address = InetAddress.getByName(host);
			this.port = port;
			this.timeout = timeout;
			this.aesKey = aesKey;
		} catch (UnknownHostException e) {
			throw new RuntimeException("getting address " + host, e);
		}
	}

	@Override
	public void connect() throws IOException {

		this.socket = new DatagramSocket();
		this.socket.setSoTimeout(timeout);
	}

	@Override
	public void disconnect() {

		if (this.socket != null) {
			this.socket.close();
		}
	}

	@Override
	public String get(String key) {

		String message = "G" + key;
		CoDeImpl ci = new CoDeImpl(aesKey);
		try {
			byte[] buf = ci.encode(message);

			DatagramPacket packet = new DatagramPacket(buf, buf.length, address, port);
			socket.send(packet);
			LOGGER.info("sent " + Arrays.toString(buf)+" - "+message);

			buf = new byte[SIZE];
			packet = new DatagramPacket(buf, SIZE);
			socket.receive(packet);

			buf = packet.getData();

			String r = ci.decode(buf);

			String f = r.replaceFirst("G", "");

			int size = f.length();

			if (size < 1 || buf[0] == '?') {
				throw new IllegalArgumentException();
			} else if (buf[0] == '!') {
				return null;
			} else {

				LOGGER.info("got " + f);
				return f;
			}

		} catch (SocketTimeoutException e) {
			throw new IllegalStateException(e);

		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void set(String key, String value) {

		String message = "S" + key + ":" + value;

		CoDeImpl ci = new CoDeImpl(aesKey);
		try {
			byte[] buf = ci.encode(message);

			DatagramPacket packet = new DatagramPacket(buf, buf.length, address, port);
			socket.send(packet);
			LOGGER.info("sent " + Arrays.toString(buf)+" - "+message);

			buf = new byte[SIZE];
			packet = new DatagramPacket(buf, SIZE);
			socket.receive(packet);

			buf = packet.getData();
			String r = ci.decode(buf);
			String f = r.replaceFirst("S", "");

			int size = f.length();

			if (size != 1 || buf[0] != '=') {
				throw new IllegalArgumentException("unexpected result " + buf[0]);
			}

		} catch (SocketTimeoutException e) {
			throw new IllegalStateException(e);

		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
}
