package m9u1.impl.cbcpeerb;

import m9u1.prob.cbcpeerb.CBCPeerFactory;
import m9u1.prob.msgpeer.MsgHandler;
import m9u1.prob.msgpeer.MsgPeer;

public class CBCPeerFactoryImpl implements CBCPeerFactory {

	@Override
	public MsgPeer createPeer(String host, int port, MsgHandler handler) {

		return new CBCPeerImpl(host, port, handler);
	}

}
