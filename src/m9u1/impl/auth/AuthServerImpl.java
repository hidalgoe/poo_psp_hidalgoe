package m9u1.impl.auth;

import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

import lib.CryptoKeys;
import m9u1.prob.auth.AuthKeys;
import m9u1.prob.auth.AuthServer;
import m9u1.prob.auth.AuthToken;
import m9u1.prob.auth.UserEntry;
import m9u1.prob.auth.UserInfo;
import m9u1.prob.auth.UserRole;

public class AuthServerImpl implements AuthServer {

	static final Logger LOGGER = Logger.getLogger(AuthServerImpl.class.getName());
	List<UserInfo> users;
	long validityMillis;
	Map<String, UserEntry> UMap;

	public AuthServerImpl(List<UserInfo> users, long validityMillis) {

		this.users = users;
		this.validityMillis = validityMillis;
		UMap = new HashMap<String, UserEntry>();

		for (int i = 0; i < users.size(); i++) {

			SecureRandom random = new SecureRandom();
			byte[] salt = new byte[16];
			random.nextBytes(salt);

			SecretKey s;
			SecretKeyFactory f;
			try {
				f = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
				KeySpec ks = new PBEKeySpec(users.get(i).getPass(), salt, 311296, 256);
				s = f.generateSecret(ks);
			} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
				throw new RuntimeException();
			}

			byte[] hash = s.getEncoded();

			UserEntry user = new UserEntry(hash, salt, users.get(i).getRole());
			UMap.put(users.get(i).getUser(), user);

		}

	}

	@Override
	public String createAccessToken(String user, String pass) {

		byte[] mixHash;
		String tdecampana = null;

		if (UMap.containsKey(user)) {

			byte[] hush = UMap.get(user).getHash();

			SecretKey s;
			SecretKeyFactory f;
			try {
				f = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
				KeySpec ks = new PBEKeySpec(pass.toCharArray(), UMap.get(user).getSalt(), 311296, 256);
				s = f.generateSecret(ks);
			} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
				throw new RuntimeException();
			}

			byte[] hush2 = s.getEncoded();

			if (Arrays.equals(hush, hush2)) {
				UserRole rolsito = UMap.get(user).getRole();
				long caducity = System.currentTimeMillis() + this.validityMillis;

				String mikol = caducity + "." + rolsito.toString();

				byte[] juntito = mikol.getBytes();

				try {
					KeyFactory kf = KeyFactory.getInstance("RSA");

					byte[] pubBytes = CryptoKeys.base64ToBytes(AuthKeys.RSA_PRIVATE);
					PrivateKey pubKey = CryptoKeys.importPrivateKey(kf, pubBytes);

					Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
					cipher.init(Cipher.ENCRYPT_MODE, pubKey);

					mixHash = cipher.doFinal(juntito);

					AuthToken token = new AuthToken(caducity, rolsito, mixHash);
					tdecampana = token.toToken();

				} catch (GeneralSecurityException e) {
					throw new RuntimeException(e);
				}
				return tdecampana;
			}else {
				LOGGER.info("no son iguales");
			}
		}
		return null;
	}

}
