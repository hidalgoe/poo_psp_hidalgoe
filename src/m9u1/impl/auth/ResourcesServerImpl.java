package m9u1.impl.auth;

import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.crypto.Cipher;

import lib.CryptoKeys;
import m9u1.prob.auth.AuthKeys;
import m9u1.prob.auth.AuthToken;
import m9u1.prob.auth.ResourceItem;
import m9u1.prob.auth.ResourceServer;
import m9u1.prob.auth.UserRole;

public class ResourcesServerImpl implements ResourceServer {

	Map<String, ResourceItem> resources;

	public ResourcesServerImpl(List<ResourceItem> items) {

		this.resources = new HashMap<>();
		for (ResourceItem item : items) {
			this.resources.put(item.getKey(), item);
		}

	}

	@Override
	public String getResource(String token, String resourceId) {

		AuthToken at = new AuthToken(token);

		long exp = at.getExpirationTime();
		UserRole r = at.getRole();
		byte[] sign = at.getSignature();

		String enviar = "";

		if (resources.containsKey(resourceId)) {

			try {
				KeyFactory kf = KeyFactory.getInstance("RSA");
				byte[] pubBytes = CryptoKeys.base64ToBytes(AuthKeys.RSA_PUBLIC);
				PublicKey pubKey = CryptoKeys.importPublicKey(kf, pubBytes);

				Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
				cipher.init(Cipher.DECRYPT_MODE, pubKey);

				byte[] mixHash = cipher.doFinal(sign);

				String str = new String(mixHash);

				String check = exp + "." + r;

				if (check.equals(str)) {

					if(exp > System.currentTimeMillis()) {
						ResourceItem item = resources.get(resourceId);

						if (item.isAdmin()) {

							if (r == UserRole.ADMIN) {
								return item.getValue();
							}else {
								throw new IllegalArgumentException();
							}
						} else {
							return item.getValue();
						}
					}else {
						throw new IllegalStateException();
					}
					
				}else {
					throw new IllegalArgumentException();
				}

			} catch (GeneralSecurityException e) {
				throw new RuntimeException(e);
			}
		}

		return null;
	}
}
