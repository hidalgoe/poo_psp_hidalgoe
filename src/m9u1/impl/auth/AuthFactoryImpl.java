package m9u1.impl.auth;

import java.util.List;

import m9u1.prob.auth.AuthFactory;
import m9u1.prob.auth.AuthServer;
import m9u1.prob.auth.ResourceItem;
import m9u1.prob.auth.ResourceServer;
import m9u1.prob.auth.UserInfo;

public class AuthFactoryImpl implements AuthFactory {

	@Override
	public AuthServer createAuthServer(List<UserInfo> users, long validityMillis) {
		return new AuthServerImpl(users, validityMillis);
	}

	@Override
	public ResourceServer createResourceServer(List<ResourceItem> items) {
		return new ResourcesServerImpl(items);
	}

}
