package m9u1.impl.tsa;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.MessageDigest;
import java.security.PrivateKey;
import java.util.logging.Logger;

import javax.crypto.Cipher;

import lib.CryptoKeys;
import m9u1.play.MessageDigestPlay;
import m9u1.prob.tsa.TSAHelper;
import m9u1.prob.tsa.TSAKeys;
import m9u3.base.BaseUDPServer;

public class TSAServerImpl extends BaseUDPServer {

	static final Logger LOGGER = Logger.getLogger(MessageDigestPlay.class.getName());

	String host;
	int port;

	public TSAServerImpl(String host, int port, int timeout) {
		super(host, port, 1500);
		this.host = host;
		this.port = port;
	}

	@Override
	protected void handle(DatagramPacket packet) {

		long tmst = TSAHelper.timestamp();

		byte[] info = new byte[packet.getLength()];
	    System.arraycopy(packet.getData(), 0, info, 0, packet.getLength());
		
		byte[] ltb = TSAHelper.longToBytes(tmst);

		byte[] mixHash;
		try {

			MessageDigest md = MessageDigest.getInstance("SHA-256");

			md.reset();
			md.update(ltb);
			md.update(info);

			byte[] digest = md.digest();

			// ENCRIPTAR

			KeyFactory kf = KeyFactory.getInstance("RSA");
			byte[] pubBytes = CryptoKeys.base64ToBytes(TSAKeys.RSA_PRIVATE);
			PrivateKey pubKey = CryptoKeys.importPrivateKey(kf, pubBytes);

			Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
			cipher.init(Cipher.ENCRYPT_MODE, pubKey);

			mixHash = cipher.doFinal(digest);

		} catch (GeneralSecurityException e) {
			throw new RuntimeException(e);
		}

		byte[] last = new byte[136];

		System.arraycopy(ltb, 0, last, 0, ltb.length);
		System.arraycopy(mixHash, 0, last, ltb.length, mixHash.length);

		try {
			DatagramPacket dp = new DatagramPacket(last, last.length, packet.getAddress(), packet.getPort());
			socket.send(dp);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}


}
