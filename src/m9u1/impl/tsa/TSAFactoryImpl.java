package m9u1.impl.tsa;

import lib.Startable;
import m9u1.prob.tsa.*;

public class TSAFactoryImpl implements TSAFactory {

	@Override
	public TSAClient createClient(String host, int port) {
		return new TSAClientImpl(host, port);
	}

	@Override
	public Startable createServer(String host, int port) {
		return new TSAServerImpl(host, port, 1500);
	}

}
