package m9u1.impl.tsa;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.util.Arrays;
import java.util.logging.Logger;

import javax.crypto.Cipher;

import lib.CryptoKeys;
import m9u1.prob.tsa.TSAClient;
import m9u1.prob.tsa.TSAHelper;
import m9u1.prob.tsa.TSAKeys;

public class TSAClientImpl implements TSAClient {

	static final Logger LOGGER = Logger.getLogger(TSAClientImpl.class.getName());

	static final int SIZE = 1460;

	private String host;
	private int port;
	private MessageDigest messageDigest;

	public TSAClientImpl(String host, int port) {

		this.host = host;
		this.port = port;

		try {
			this.messageDigest = MessageDigest.getInstance("SHA-256");
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		}
	}

	private byte[] hash(File file) throws IOException {

		this.messageDigest.reset();

		try (InputStream is = new FileInputStream(file)) {

			byte[] buf = new byte[8192];
			int count;
			while ((count = is.read(buf)) != -1) {
				this.messageDigest.update(buf, 0, count);
			}

			return this.messageDigest.digest();
		}
	}

	@Override
	public Stamp stamp(File file) throws IOException {

		// CONNECTS TO SERVER

		try (DatagramSocket socket = new DatagramSocket()) {

			byte[] fileHash = hash(file);
			LOGGER.info("file hash: " + CryptoKeys.pretty(fileHash, 16, 64));

			// sends the file hash

			InetAddress address = InetAddress.getByName(host);
			DatagramPacket packet = new DatagramPacket(fileHash, fileHash.length, address, port);
			socket.send(packet);

			// receives the timestamp (8 bytes) and the signature (rest of the data)

			byte[] buf = new byte[SIZE];
			packet = new DatagramPacket(buf, SIZE);
			socket.receive(packet);

			byte[] timestamp = new byte[Long.BYTES];
			System.arraycopy(buf, 0, timestamp, 0, Long.BYTES);
			byte[] signature = new byte[packet.getLength() - Long.BYTES];
			System.arraycopy(buf, Long.BYTES, signature, 0, packet.getLength() - Long.BYTES);

			return new Stamp() {

				@Override
				public long timestamp() {
					return TSAHelper.bytesToLong(timestamp);
				}

				@Override
				public byte[] signature() {
					return signature;
				}
			};
		}
	}

	@Override
	public boolean verify(File file, Stamp stamp) throws IOException, GeneralSecurityException {

		// OFFLINE

		byte[] fileHash = hash(file);

		KeyFactory kf = KeyFactory.getInstance("RSA");
		byte[] pubBytes = CryptoKeys.base64ToBytes(TSAKeys.RSA_PUBLIC);
		PublicKey pubKey = CryptoKeys.importPublicKey(kf, pubBytes);

		// deciphers the signature using the public key

		Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
		cipher.init(Cipher.DECRYPT_MODE, pubKey);

		byte[] mixHash = cipher.doFinal(stamp.signature());

		// calculates the hash for the timestamp + file hash (mix hash)

		byte[] timestamp = TSAHelper.longToBytes(stamp.timestamp());

		messageDigest.reset();
		messageDigest.update(timestamp); // add the timestamp
		messageDigest.update(fileHash); // add the incoming hash

		byte[] calcMixHash = messageDigest.digest();

		// compares the calculated hash with the received one

		LOGGER.info("calchash: " + CryptoKeys.pretty(calcMixHash, 16, 64));
		LOGGER.info("received: " + CryptoKeys.pretty(mixHash, 16, 64));

		return Arrays.equals(mixHash, calcMixHash);
	}

}
